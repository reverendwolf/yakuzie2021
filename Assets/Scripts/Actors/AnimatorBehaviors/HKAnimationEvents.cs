﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HKAnimationEvents : MonoBehaviour
{
    //HKPlayer player;
    IAnimEvents iae;

    // Start is called before the first frame update
    void Start()
    {
        //player = GetComponentInParent<HKPlayer>();
        iae = GetComponentInParent<IAnimEvents>();
    }

    public void MovementLock(int input)
    {
        //player.LockMove(input != 0);
        iae.MovementLock(input != 0);
    }

    public void ActionLock(int input)
    {
        //player.LockMove(input != 0);
        iae.ActionLock(input != 0);
    }

    public void Hit(Object messageObject)
    {
        if (messageObject != null)
        {
            Debug.Log(messageObject.name);
        }

        //player.Hit();
        iae.Hit(messageObject);
    }

    public void Lunge(float strength)
    {
        //player.MoveLunge(strength);
        iae.Lunge(strength);
    }

    public void Image()
    {
        //player.AfterImage();
        iae.Image();
    }

    public void Surge()
    {
        //player.Surge();
        iae.Surge();
    }

    public void Step()
    {
        iae.Step();
    }
}
