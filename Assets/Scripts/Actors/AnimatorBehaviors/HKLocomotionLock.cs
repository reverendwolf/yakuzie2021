﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HKLocomotionLock : StateMachineBehaviour
{
    public enum LockSet
    {
        Unlock,
        Lock,
        Ignore
    }
    [SerializeField] LockSet onEnterMove;
    [SerializeField] LockSet onEnterAction;
    [SerializeField] LockSet onExitMove;
    [SerializeField] LockSet onExitAction;

    public override void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        //Debug.Log("Attack Start!");
        if (onEnterMove != LockSet.Ignore) animator.GetComponent<HKAnimationEvents>().MovementLock((int)onEnterMove);
        if (onEnterAction != LockSet.Ignore) animator.GetComponent<HKAnimationEvents>().ActionLock((int)onEnterAction);
    }

    public override void OnStateExit(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        //Debug.Log("Attack End!");
        if (onExitMove != LockSet.Ignore) animator.GetComponent<HKAnimationEvents>().MovementLock((int)onExitMove);
        if (onExitAction != LockSet.Ignore) animator.GetComponent<HKAnimationEvents>().ActionLock((int)onExitAction);
    }
}
