﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(NavmeshMover))]
public class ZAIWanderer : ZeldaAIBase
{

    NavmeshMover mover;
    [Header("Wander Settings")]
    [SerializeField] float wanderDistance;
    [SerializeField] float wanderMinTime;
    [SerializeField] float wanderMaxTime;
    [SerializeField] bool stayHome;

    private void Awake()
    {
        mover = GetComponent<NavmeshMover>();
    }

    protected override void UpdateBehaviour()
    {
        Wander(wanderDistance, Random.Range(wanderMinTime, wanderMaxTime), stayHome, mover);
    }

    protected override void CancelBehaviours()
    {
        mover.ForceStop();
    }

    protected override void PauseBehaviours(bool pause)
    {
        mover.Pause(pause);
    }

    protected override void OnIncomingAttack(AttackData attack)
    {
        mover.ForceStop();
        currentBehaviour = Knockback(mover, attack, attackable);
    }

    public void OnDrawGizmosSelected()
    {
        Gizmos.color = new Color(0f,1f,0f,0.25f);
        Gizmos.DrawSphere(stayHome ? Application.isPlaying ? home : transform.position : transform.position, wanderDistance);
    }
}
