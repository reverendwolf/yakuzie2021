﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class ZeldaAIBase : MonoBehaviour
{
    [SerializeField] string debugBehaviour;
    protected Vector3 home;
    protected bool paused;
    protected Attackable attackable;

    protected IEnumerator currentBehaviour;

    protected IEnumerator Sleep(float sleepTime)
    {
        debugBehaviour = "Sleep";
        while(sleepTime > 0f)
        {
            sleepTime -= Time.deltaTime;
            yield return null;
        }
    }

    protected IEnumerator MoveDirection(NavmeshMover mover, Vector2 direction, float timer)
    {
        debugBehaviour = "Move Direction";
        while(timer >= 0f)
        {
            Debug.DrawRay(mover.transform.position + Vector3.up, new Vector3(direction.x, 0, direction.y), Color.green, Time.deltaTime);
            mover.MoveDirection(direction);
            timer -= Time.deltaTime;
            yield return null;
        }
    }

    protected IEnumerator MoveToPoint(NavmeshMover mover, Vector3 point, float timer)
    {
        debugBehaviour = "Move To Point";
        mover.MoveTo(point);
        Debug.DrawRay(point, Vector3.up * 3, Color.green, timer);

        while(mover.WalkingTo)
        {
            yield return null;
        }

        while (timer >= 0f)
        {
            timer -= Time.deltaTime;
            yield return null;
        }
    }

    protected IEnumerator ChaseTransform(NavmeshMover mover, Transform target, float timer)
    {
        debugBehaviour = "Chase Transform";

        mover.MoveTo(target.position);
        Debug.DrawRay(target.position, Vector3.up * 3, Color.red, timer);

        while(timer >= 0f)
        {
            timer -= Time.deltaTime;
            yield return null;
        }
    }

    protected IEnumerator Knockback(NavmeshMover mover, AttackData attack, Attackable attackable = null)
    {
        attackable.IFrames(0.55f);
        if (attackable != null && attackable.Heavy)
        {
            attackable.EvaluateLife();
            yield break;
        }

        mover.PushDirection((transform.position - attack.sourcePosition).ConvertToV2(), 3.5f, 0.35f);

        while(mover.Pushing)
        {
            yield return null;
        }

        if(attackable != null)
        {
            attackable.EvaluateLife();
        }
    }

    protected void Wander(float wanderDistance, float wanderTime, bool stayHome, NavmeshMover mover)
    {
        Vector2 direction = Random.insideUnitCircle.normalized * wanderDistance;

        Vector3 target = stayHome ? new Vector3(home.x + direction.x, home.y, home.z + direction.y) : new Vector3(transform.position.x + direction.x, transform.position.y, transform.position.z + direction.y);

        //direction = (transform.position - target).ConvertToV2();

        //currentBehaviour = MoveDirection(mover, direction, wanderTime);
        
        currentBehaviour = MoveToPoint(mover, target, wanderTime);
    }

    public void Start()
    {
        home = transform.position;
        attackable = GetComponent<Attackable>();
        if (attackable != null)
        {
            attackable.OnAttack += OnIncomingAttack;
        }
    }

    public void Update()
    {
        if(!GameMaster.I.PauseState) Tick();
    }

    protected virtual void UpdateBehaviour()
    {
        currentBehaviour = Sleep(1f);
    }

    protected virtual void CancelBehaviours()
    {

    }

    protected virtual void PauseBehaviours(bool pause)
    {

    }

    protected virtual void OnIncomingAttack(AttackData attack)
    {

    }

    protected virtual void KnockbackComplete(NavmeshMover mover)
    {

    }

    public virtual void Tick()
    {
        if(currentBehaviour == null || currentBehaviour.MoveNext() == false)
        {
            UpdateBehaviour();
        }
    }

    private void OnEnable()
    {
        GameMaster.OnPauseGame += PauseInput;
        
    }

    private void OnDisable()
    {
        GameMaster.OnPauseGame -= PauseInput;
    }

    void PauseInput(bool p)
    {
        paused = p;
        PauseBehaviours(paused);
    }
}
