﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(NavmeshMover))]
public class ZAICharger : ZeldaAIBase
{
    public enum IdleBehaviour
    {
        SLEEP, PATROL, WANDER
    }

    public enum CurrentState
    {
        IDLE, SEEN, LOST, FORGOT
    }

    [SerializeField] IdleBehaviour idle;
    [SerializeField] CurrentState currentState;
    NavmeshMover mover;
    CombatSensor sensor;
    [Header("Patrol/Wander Settings")]
    [SerializeField] float wanderDistance = 2f;
    [SerializeField] float wanderMinTime = 1f;
    [SerializeField] float wanderMaxTime = 3f;
    [Header("Charge Settings")]
    [SerializeField] float memoryTime = 2f;
    [SerializeField] float chargeSpeed = 3f;
    [SerializeField] float chaseTime = 6f;

    float ChaseTime => Random.Range(chaseTime * 0.25f, chaseTime);

    Vector3 lastPlayerPosition;
    Transform playerReference;
    float memory;
    float chase;

    private void Awake()
    {
        mover = GetComponent<NavmeshMover>();
        sensor = GetComponentInChildren<CombatSensor>();

        if(sensor != null)
        {
            sensor.OnSeePlayer += OnPlayerSeen;
            sensor.OnLosePlayer += OnPlayerLost;
        }
    }

    protected override void UpdateBehaviour()
    {
        //Wander(wanderDistance, Random.Range(wanderMinTime, wanderMaxTime), stayHome, mover);
        switch(currentState)
        {
            case CurrentState.IDLE:

                switch(idle)
                {
                    case IdleBehaviour.SLEEP:
                        currentBehaviour = Sleep(3f);
                        break;
                    case IdleBehaviour.PATROL:
                        Wander(wanderDistance, Random.Range(wanderMinTime, wanderMaxTime), true, mover);
                        break;
                    case IdleBehaviour.WANDER:
                        Wander(wanderDistance, Random.Range(wanderMinTime, wanderMaxTime), false, mover);
                        break;
                }

                break;
            case CurrentState.SEEN:

                currentBehaviour = ChaseTransform(mover, playerReference, 0.15f);

                break;
            case CurrentState.LOST:

                currentBehaviour = MoveToPoint(mover, lastPlayerPosition, 3f);

                break;
            case CurrentState.FORGOT:

                currentBehaviour = MoveToPoint(mover, home, 1f);

                break;
        }
    }

    public override void Tick()
    {
        base.Tick();

        if(currentState == CurrentState.LOST)
        {
            memory -= Time.deltaTime;
            if(memory < 0f)
            {
                currentBehaviour = null;
                currentState = CurrentState.FORGOT;
                mover.ResetSpeed();
                memory = 0f;
            }
        }

        if(currentState == CurrentState.FORGOT && Vector3.SqrMagnitude(home - transform.position) < 1f)
        {
            currentBehaviour = null;
            currentState = CurrentState.IDLE;
        }

        if(currentState == CurrentState.SEEN)
        {
            chase -= Time.deltaTime;
            if(chase < 0f)
            {
                //taunt?
                currentBehaviour = Sleep(1.5f);
                chase = ChaseTime + 1.5f;
            }
        }

        if(Vector3.SqrMagnitude(transform.position - home) > (15 * 15))
        {
            currentBehaviour = null;
            currentState = CurrentState.FORGOT;
            mover.ResetSpeed();
            memory = 0f;
        }
    }

    protected override void CancelBehaviours()
    {
        mover.ForceStop();
    }

    protected override void PauseBehaviours(bool pause)
    {
        mover.Pause(pause);
    }

    protected override void OnIncomingAttack(AttackData attack)
    {
        mover.ForceStop();
        currentBehaviour = Knockback(mover, attack, attackable);
        if(currentState == CurrentState.SEEN)
        {
            //chase = ChaseTime;
        }
        else
        {
            lastPlayerPosition = attack.ownerTransform.position;
            currentBehaviour = null;
            currentState = CurrentState.LOST;
            mover.SetSpeed((chargeSpeed + mover.AgentSpeed) * 0.5f);
            memory = memoryTime * 1.5f;
        }
    }

    public void OnDrawGizmosSelected()
    {
        //Gizmos.color = new Color(0f, 1f, 0f, 0.25f);
        //Gizmos.DrawSphere(stayHome ? Application.isPlaying ? home : transform.position : transform.position, wanderDistance);
    }

    private void OnPlayerSeen(Attackable player)
    {
        Debug.Log(this.gameObject.name + " saw the player!");
        currentBehaviour = null;
        playerReference = player.transform;
        currentState = CurrentState.SEEN;
        mover.SetSpeed(chargeSpeed);
        chase = ChaseTime;
    }

    private void OnPlayerLost(Attackable player)
    {
        Debug.Log(this.gameObject.name + " lost the player!");
        currentBehaviour = null;
        lastPlayerPosition = playerReference.position;
        currentState = CurrentState.LOST;
        memory = memoryTime;
    }
}
