﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MapObjectSensor : MonoBehaviour
{
    public BaseMapObject First => checkables[0];
    public bool HasList => checkables.Count > 0;

    [SerializeField] List<BaseMapObject> checkables = new List<BaseMapObject>();

    private void OnTriggerEnter(Collider other)
    {
        BaseMapObject bmo = other.GetComponent<BaseMapObject>();
        if (bmo == null) bmo = other.GetComponentInParent<BaseMapObject>();

        if(bmo != null)
        {
            if(bmo.InteractionType == BaseMapObject.Interaction.Manual)
            {
                //Debug.Log("Adding " + other.gameObject.name);
                checkables.Add(other.GetComponent<BaseMapObject>());
                UpdateInteractionLabel();
            }
            else if(bmo.InteractionType == BaseMapObject.Interaction.Auto)
            {
                bmo.Interact();
            }
        }
    }

    private void OnTriggerExit(Collider other)
    {
        BaseMapObject bmo = other.GetComponent<BaseMapObject>();
        if (bmo == null) bmo = other.GetComponentInParent<BaseMapObject>();

        if (bmo && checkables.Contains(bmo))
        {
            //Debug.Log("Removing " + other.gameObject.name);
            bmo.Leave();
            checkables.Remove(other.GetComponent<BaseMapObject>());
            UpdateInteractionLabel();
        }
        else if (bmo && bmo.InteractionType == BaseMapObject.Interaction.Auto)
        {
            bmo.Leave();
        }
    }

    void UpdateInteractionLabel()
    {
        if(HasList)
        {
            //show verb on first entry
            UIMaster.I.ShowVerb(First.ActionLabel);
        }
        else
        {
            //hide interaction label
            UIMaster.I.HideVerb();
        }
    }

    void ValidateLists()
    {
        for (int i = checkables.Count - 1; i >= 0; i--)
        {
            if(checkables[i] == null || checkables[i].gameObject.activeInHierarchy == false)
            {
                checkables.RemoveAt(i);
                UpdateInteractionLabel();
            }
        }
    }

    private void LateUpdate()
    {
        if(HasList) ValidateLists();
    }
}
