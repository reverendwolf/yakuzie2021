﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Collider))]
public class CombatSensor : MonoBehaviour
{
    public delegate void SeeAttackable(Attackable attackable);


    public event SeeAttackable OnSeeMonster;
    public event SeeAttackable OnSeePlayer;
    public event SeeAttackable OnSeeObject;

    public event SeeAttackable OnLoseMonster;
    public event SeeAttackable OnLosePlayer;
    public event SeeAttackable OnLoseObject;

    Attackable owner;

    Collider c;

    [SerializeField] List<Attackable> attackables = new List<Attackable>();
    //public Attackable First { get { ValidateLists(); return attackables[0]; } }
    public Attackable[] Attackables { get { ValidateLists(); return attackables.ToArray(); } }
    //public int Count { get { ValidateLists(); return attackables.Count; } }

    public int MonsterCount
    {
        get
        {
            ValidateLists();
            int _return = 0;
            foreach(Attackable a in attackables)
            {
                if (a.gameObject.CompareTag("Monster"))
                    _return++;
            }
            return _return;
        }
    }

    private void Start()
    {
        c = GetComponent<Collider>();
        c.isTrigger = true;
    }

    public void RegisterOwner(Attackable owner)
    {
        this.owner = owner;
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.GetComponent<Attackable>() == null || (owner != null && other.GetComponent<Attackable>() == owner)) return;

        if(other.CompareTag("Monster"))
        {
            OnSeeMonster?.Invoke(other.GetComponent<Attackable>());
        }
        else if(other.CompareTag("Player"))
        {
            OnSeePlayer?.Invoke(other.GetComponent<Attackable>());
            
        }
        else if(other.CompareTag("Object"))
        {
            OnSeeObject?.Invoke(other.GetComponent<Attackable>());
        }
        attackables.Add(other.GetComponent<Attackable>());
        ValidateLists();
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.CompareTag("Monster") && attackables.Contains(other.GetComponent<Attackable>()))
        {
            OnLoseMonster?.Invoke(other.GetComponent<Attackable>());
        }
        else if (other.CompareTag("Player") && attackables.Contains(other.GetComponent<Attackable>()))
        {
            OnLosePlayer?.Invoke(other.GetComponent<Attackable>());
        }
        else if (other.CompareTag("Object") && attackables.Contains(other.GetComponent<Attackable>()))
        {
            OnLoseObject?.Invoke(other.GetComponent<Attackable>());
        }
        attackables.Remove(other.GetComponent<Attackable>());
    }

    void ValidateLists()
    {
        for (int i = attackables.Count - 1; i >= 0; i--)
        {
            if(attackables[i] == null || attackables[i].gameObject.activeInHierarchy == false)
            {
                attackables.RemoveAt(i);
            }
        }
    }
}
