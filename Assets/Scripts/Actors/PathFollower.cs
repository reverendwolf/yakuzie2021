﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(NavmeshMover))]
public class PathFollower : MonoBehaviour
{

    NavmeshMover mover;
    Vector3 home;

    [SerializeField] SimplePath path = new SimplePath();
    [SerializeField] bool randomStartingIndex;

    int currentIndex;
    float wait = 0f;
    bool justWaited;
    bool paused;
    private void Awake()
    {
        home = transform.position;
        mover = GetComponent<NavmeshMover>();
        if(randomStartingIndex)
        {
            currentIndex = Random.Range(0, path.Length);
        }
    }

    private void OnEnable()
    {
        GameMaster.OnPauseGame += OnPause;
    }

    private void OnDisable()
    {
        GameMaster.OnPauseGame -= OnPause;
    }

    // Update is called once per frame
    void LateUpdate()
    {
        if (paused) return;

        if (mover.WalkingTo)
        {
            return;
        }
        else if(wait > 0f)
        {
            wait -= Time.deltaTime;
            //Debug.Log("Wait: " + wait);
            return;
        }
        else
        {
            if(justWaited == false && path.GetRandomWait(currentIndex))
            {
                //Debug.Log("Wait");
                justWaited = true;
                wait = path.GetWaitTime(currentIndex);
            }
            else
            {
               // Debug.Log("Go");
                currentIndex++;
                currentIndex %= path.Length;
                mover.MoveTo(home + path.GetRandomPoint(currentIndex));
                justWaited = false;
            }
        }
    }

    private void OnDrawGizmosSelected()
    {
        Vector3 start = Application.isPlaying ? home : transform.position;

        if (path != null && path.Length > 0)
        {
            for (int i = 0; i < path.Length; i++)
            {
                if(i == 0) Gizmos.color = Color.blue;
                else if (i == 1) Gizmos.color = Color.yellow;
                else Gizmos.color = Color.green;

                //Gizmos.color = Color.green;
                Gizmos.DrawSphere(start + path.GetPoint(i) + (Vector3.up * (i==0 ? 3 : 1)), .75f);
                Gizmos.DrawWireSphere(start + path.GetPoint(i), path.GetDistance(i));

                Gizmos.color = Color.black;
                Gizmos.DrawLine(start + path.GetPoint(i), start + path.GetPoint(i) + (Vector3.up * (i == 0 ? 3 : 1)));

                if (i > 0) Gizmos.DrawLine(start + path.GetPoint(i), start + path.GetPoint(i - 1));

                if (i == 0 && path.Length > 2) Gizmos.DrawLine(start + path.GetPoint(i), start + path.GetPoint(path.Length - 1));

            }
        }

    }

    void OnPause(bool p)
    {
        paused = p;
        mover?.Pause(p);
    }
}
