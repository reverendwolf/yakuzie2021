﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu()]
public class MissionGoal : ScriptableObject
{
    public delegate void GoalEvent(MissionGoal goal);

    public event GoalEvent OnCompleteGoal;
    public event GoalEvent OnProgressGoal;

    [SerializeField] string goalName; //nice name
    [SerializeField] [TextArea] string goalDesc; //in game description
    int goalProg; //the progress pips towards completing this goal
    [SerializeField] int goalMaxProg; //the number of pips needed to complete the goal

    public string GoalName => goalName;
    public string GoalDesc => goalDesc;
    public string GoalText => string.Format("({0}/{1})",goalProg, goalMaxProg);
    public int Progress => goalProg;
    public int Maximum => goalMaxProg;
    public bool Completed => (goalProg >= goalMaxProg);

    public void InitGoalProgress()
    {
        goalProg = 0;
    }

    public void SetComplete()
    {
        goalProg = goalMaxProg;
    }

    public void SetProgress(int progress)
    {
        goalProg = progress;
        GoalCheck();
    }

    public void AddProgress(int progress)
    {
        goalProg += progress;
        GoalCheck();
    }

    void GoalCheck()
    {
        if (goalProg >= goalMaxProg)
        {
            Debug.Log("Goal Complete! (" + goalName + ")", this);
            OnCompleteGoal?.Invoke(this);
        }
    }

    void OnValidate()
    {
        if (goalMaxProg <= 0) goalMaxProg = 1;
    }
}
