﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu()]
public class GameSaveSlots : ScriptableObject
{
    [SerializeField] GameData slot1;
    [SerializeField] GameData slot2;
    [SerializeField] GameData slot3;
    [SerializeField] string slot1Json;
    [SerializeField] string slot2Json;
    [SerializeField] string slot3Json;

    public void WriteSaveToJson()
    {
        slot1Json = slot1.Serialize();
    }
}
