﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class SessionData
{
    [System.Serializable]
    public class SimpleQuestObjective
    {
        [SerializeField] string name;
        [SerializeField] [TextArea] string summary;

        public string Name => name;
        public string Summary => summary;
    }

    [System.Serializable]
    public class QuestChain
    {
        public enum QuestStatus
        {
            Undiscovered,
            Active,
            Complete
        }
        [SerializeField] SimpleQuestObjective summary;
        [SerializeField] SimpleQuestObjective[] chain;
        [SerializeField] int progress;
        [SerializeField] QuestStatus status;
        [SerializeField] int zenny;

        public SimpleQuestObjective Summary => summary;
        public SimpleQuestObjective CurrentObjective => chain[progress];
        public int Progress => progress;
        public QuestStatus Status => status;
        public int Reward => zenny;

        public void SetProgress(int set)
        {
            set = Mathf.Clamp(set, 0, chain.Length - 1);
            progress = set;
        }

        public void SetStatus(QuestStatus set)
        {
            status = set;
        }
    }

    [SerializeField] QuestChain sessionQuest;
    [SerializeField] QuestChain[] sideQuests;
}
