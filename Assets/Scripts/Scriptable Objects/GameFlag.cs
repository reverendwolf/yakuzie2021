﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu()]
public class GameFlag : ScriptableObject {}

[System.Serializable]
public class GameFlagCheck
{
    public GameFlag flag;
    public Helper.Comparison operation;
    public byte value;
}

[System.Serializable]
public class GameFlagSet
{
    public GameFlag flag;
    public Helper.Operation operation;
    public byte value;
}