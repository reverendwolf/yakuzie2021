﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class GameData
{
    public delegate void GDEvent();
    public event GDEvent OnDataUpdate;

    [SerializeField] int currentSession;
    [SerializeField] int sessionPhase;
    [SerializeField] int zenny = 0;
    [SerializeField] int mythril;
    [SerializeField] int crystal;
    [SerializeField] [Range(1, 4)] int weaponLevel = 1;
    [SerializeField] [Range(1, 4)] int healthLevel = 1;
    [SerializeField] [Range(1, 4)] int barLevel = 1;

    public bool[] Skills => new bool[] { true, true, (special == Helper.SkillStatus.Purchased), (heavy == Helper.SkillStatus.Purchased), (cast == Helper.SkillStatus.Purchased), (ultimate == Helper.SkillStatus.Purchased) };

    [SerializeField] Helper.SkillStatus special; //special weapons on triangle
    [SerializeField] Helper.SkillStatus heavy; //super moves on r1
    [SerializeField] Helper.SkillStatus cast; //cast move on l1
    [SerializeField] Helper.SkillStatus ultimate; //ultimate move on l2

    public Helper.SkillStatus[] SkillStatus => new Helper.SkillStatus[] {special, heavy, cast, ultimate};

    int[] meleeValues = new int[] { 1, 2, 4, 8 };
    int[] healthValues = new int[] { 4, 5, 6, 8 };
    int[] barValues = new int[] { 10, 13, 16, 20};

    Dictionary<string, byte> gameState = new Dictionary<string, byte>();

    public int CurrentSession => currentSession;
    public int SessionPhase => sessionPhase;
    public int Zenny => zenny;
    public int Mythril => mythril;
    public int Crystal => crystal;
    public Dictionary<string,byte> GameState => gameState;

    public int[] ShardLevels => new int[] { weaponLevel, healthLevel, barLevel};

    public int MeleeDamage => meleeValues[weaponLevel - 1];

    public int MaxHealth => healthValues[healthLevel - 1];

    public int MaxBar => barValues[barLevel - 1];

    public string Serialize()
    {
        return JsonUtility.ToJson(this);
    }

    public byte GetFlag(string flag)
    {
        if(gameState.ContainsKey(flag))
        {
            return gameState[flag];
        }
        else
        {
            gameState.Add(flag, 0);
            OnDataUpdate?.Invoke();
            return 0;
        }
    }

    public void SetFlag(string flag, byte value)
    {
        Debug.Log("Set " + flag + " to " + value);
        if(gameState.ContainsKey(flag))
        {
            gameState[flag] = value;
        }
        else
        {
            gameState.Add(flag, value);
        }
        OnDataUpdate?.Invoke();
    }

    public void ManualOnDataUpdate()
    {
        OnDataUpdate?.Invoke();
    }
}
