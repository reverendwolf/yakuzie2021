﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class HKPlayerData
{
    [SerializeField] GameObject playerObject;
    public GameObject PlayerObject => playerObject;

    [SerializeField] string displayName;
    [SerializeField] string jobName;
    [SerializeField] [TextArea] string description;

    public string DisplayName => displayName;
    public string JobName => jobName;
    public string Description => description;

    [Header("Key Items")]

    [SerializeField] KeyItem melee;
    [SerializeField] KeyItem special;
    [SerializeField] KeyItem super;
    [SerializeField] KeyItem heal;
    [SerializeField] KeyItem cast;
    [SerializeField] KeyItem ultimate;

    [Header("Attack Moves")]
    [SerializeField] KeyItem meleeAttack;
    [SerializeField] KeyItem meleeCharged;

    [SerializeField] KeyItem specialAttack;
    [SerializeField] KeyItem specialCharged;

    [SerializeField] KeyItem meleeSuper;
    [SerializeField] KeyItem specialSuper;

    [SerializeField] KeyItem spellAttack;

    [SerializeField] KeyItem ultimateAttack;

    public List<KeyItem> KeyItems => new List<KeyItem> { melee, heal, special, super, cast, ultimate };
    public KeyItem[] Attacks => new KeyItem[] { meleeAttack, meleeCharged, specialAttack, specialCharged, meleeSuper, specialSuper, spellAttack, ultimateAttack };
}