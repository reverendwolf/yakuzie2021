﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu()]
public class KeyItem : ScriptableObject
{
    [SerializeField] Texture2D displayIcon;
    [SerializeField] string displayName;
    [SerializeField] [TextArea] string description;

    public Texture2D DisplayIcon => displayIcon;
    public string DisplayName => displayName;
    public string Description => description;
}
