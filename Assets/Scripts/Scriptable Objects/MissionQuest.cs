﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu()]
public class MissionQuest : ScriptableObject
{
    public enum ObjectiveStatus
    {
        Inactive,
        Active,
        Complete
    }

    [System.Serializable]
    public class MissionObjective
    {
        public string Title => objectiveTitle;
        public string Description => objectiveDescription;
        public ObjectiveStatus Status => objectiveStatus;
        public int Exp => expReward;
        public int Money => moneyReward;

        [SerializeField] string objectiveTitle;
        [SerializeField] [TextArea] string objectiveDescription;
        [SerializeField] ObjectiveStatus objectiveStatus;
        [SerializeField] int expReward;
        [SerializeField] int moneyReward;

        public bool SetObjectiveStatus(ObjectiveStatus newStatus)
        {
            if((int) newStatus > (int)objectiveStatus)
            {
                objectiveStatus = newStatus;
                return true;
            }
            return false;
        }

        public void Initialize()
        {
            objectiveStatus = ObjectiveStatus.Inactive;
        }

    }

    public delegate void MissionEvent();
    public event MissionEvent ObjectivesUpdated;


    [SerializeField] string missionName;
    [SerializeField] string missionSummary;
    [SerializeField] MissionObjective[] missionObjectives;
    [SerializeField] MissionObjective[] optionalObjectives;

    public string MissionName => missionName;
    public string Missionsummary => missionSummary;
    public MissionObjective[] Objectives => missionObjectives;
    public MissionObjective[] SideQuests => optionalObjectives;

    public void UpdateObjective(int objectiveID, ObjectiveStatus newStatus)
    {
        bool update = false;
        if(objectiveID < missionObjectives.Length)
        {
            update = missionObjectives[objectiveID].SetObjectiveStatus(newStatus);
        }

        if (update) ObjectivesUpdated?.Invoke();
        //Debug.Log("ObjectiveID " + objectiveID + (update ? "" : " NOT") + " updated!");
    }

    public void UpdateSideQuest(int objectiveID, ObjectiveStatus newStatus)
    {
        bool update = false;
        if (objectiveID < optionalObjectives.Length)
        {
            update = optionalObjectives[objectiveID].SetObjectiveStatus(newStatus);
        }

        if (update) ObjectivesUpdated?.Invoke();
    }

    public void InitializeQuest()
    {
        for (int i = 0; i < missionObjectives.Length; i++)
        {
            missionObjectives[i].Initialize();
        }

        for (int i = 0; i < optionalObjectives.Length; i++)
        {
            optionalObjectives[i].Initialize();
        }


        //if (missionObjectives.Length > 0) missionObjectives[0].SetObjectiveStatus(ObjectiveStatus.Active);
    }

    public string GetActiveObjectives()
    {
        string _return = "";
        string white = "<color=#FFFFFF><b>";
        string gray = "<color=#B0B0B0><i>";
        string endColor = "</color>";
        for (int i = 0; i < missionObjectives.Length; i++)
        {
            if(missionObjectives[i].Status == ObjectiveStatus.Active)
            {
                _return += white + missionObjectives[i].Title + endColor + "</b>\n";
                _return += gray + missionObjectives[i].Description + endColor + "</i>\n\n";
            }
        }
        return _return;
    }

    public string[] GetAllObjectiveNames()
    {
        string[] _return = new string[missionObjectives.Length];
        for (int i = 0; i < _return.Length; i++)
        {
            _return[i] = missionObjectives[i].Title;
        }
        return _return;
    }
}
