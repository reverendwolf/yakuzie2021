﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MOWhisper : BaseMapObject
{
    public override bool FireOnce => false;
    public override Interaction InteractionType => Interaction.Auto;
    float cooldown;
    [SerializeField] [TextArea] string whisperMessage;


    public override void Interact()
    {
        if (cooldown > 0f) return;
        UIMaster.I.ShowWhisper(whisperMessage, transform.GetChild(0).position);
        if(GetComponent<AudioSource>())
        {
            GetComponent<AudioSource>().Play();
        }
        cooldown = 5f;
    }

    private void Update()
    {
        if (cooldown >= 0f) cooldown -= Time.deltaTime;
    }

}
