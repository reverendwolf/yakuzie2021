﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Fungus;

public class MOStory : BaseMapObject
{

    [SerializeField] GUIDisplayVerb.PossibleVerbs verb;
    public override GUIDisplayVerb.PossibleVerbs ActionLabel => verb;
    [SerializeField] bool fireOnce;
    public override bool FireOnce => fireOnce;
    [SerializeField] Interaction interaction;
    public override Interaction InteractionType => this.interaction;
    public Flowchart Chart => (customFlowchart != null) ? customFlowchart : MapMaster.I.MissionChart;
    public string Slug => (customSlug == "") ? gameObject.name : customSlug;
    [SerializeField] Flowchart customFlowchart;
    [SerializeField] string customSlug;

    public override void Interact()
    {
        //Debug.Log(ActionLabel + ": " + gameObject.name, this);
        GM.LaunchConversation(this);
        base.Interact();
    }
}
