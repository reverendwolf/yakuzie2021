﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BaseMapObject : MonoBehaviour
{
    public enum Interaction { Manual, Auto}

    public virtual GUIDisplayVerb.PossibleVerbs ActionLabel => GUIDisplayVerb.PossibleVerbs.Look;
    public virtual bool FireOnce => false;
    public virtual Interaction InteractionType => Interaction.Auto;
    protected GameMaster GM => GameMaster.I;

    public virtual void Interact()
    {
        if (FireOnce) gameObject.SetActive(false);
    }

    public virtual void Leave()
    {
        //Debug.Log("Leave Called");
    }
}
