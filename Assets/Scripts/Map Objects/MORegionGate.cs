﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MORegionGate : BaseMapObject
{
    public override GUIDisplayVerb.PossibleVerbs ActionLabel => GUIDisplayVerb.PossibleVerbs.Go;
    public MapRegion TargetRegion => targetRegion;
    public Vector3 TargetPosition => targetPosition.position;
    [SerializeField] MapRegion targetRegion;
    [SerializeField] Transform targetPosition;
    [SerializeField] Interaction interaction;
    public override Interaction InteractionType => this.interaction;
    public override bool FireOnce => false;

    public override void Interact()
    {
        //base.Interact();
        GM.RegionTransition(targetRegion, targetPosition.position);
    }

    private void OnDrawGizmos()
    {
        
        if (targetPosition != null)
        {
            Gizmos.color = Color.green;
            Gizmos.DrawLine(this.transform.position, TargetPosition);
            Gizmos.DrawLine(TargetPosition, TargetPosition + Vector3.up);
            Gizmos.DrawWireCube(TargetPosition + Vector3.up, Vector3.one * 0.5f);
            Gizmos.color = Color.red;
            Gizmos.DrawLine(this.transform.position, this.transform.position + Vector3.up);
            Gizmos.DrawWireSphere(this.transform.position + Vector3.up, 0.5f);
        }
        
    }
}
