﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class MOStaticEncounter : BaseMapObject
{
    public override bool FireOnce => true;

    [SerializeField] string startFlowchart; //a flowchart to call when starting the encounter
    [SerializeField] string endFlowchart; //a flowchart to call when the encounter is considered over
    Fungus.Flowchart FC => MapMaster.I.MissionChart;
    Cinemachine.CinemachineVirtualCamera vc;
    [SerializeField] Attackable[] encounterMembers;
    [SerializeField] UnityEngine.AI.NavMeshObstacle[] obstacles;

    Coroutine wait;
    bool encounterComplete;
    bool running;

    private void Start()
    {
        vc = GetComponentInChildren<Cinemachine.CinemachineVirtualCamera>();
        encounterMembers = GetComponentsInChildren<Attackable>(true);
        obstacles = GetComponentsInChildren<UnityEngine.AI.NavMeshObstacle>(true);
        ToggleObstacles(false);
    }

    public override void Interact()
    {
        //base.Interact();
        if (running) return;
        running = true;
        Debug.Log("Interact Static Encounter: " + gameObject.name, this);
        if(startFlowchart != "" && FC.HasBlock(startFlowchart))
        {
            GM.LaunchConversation(FC, startFlowchart);
            wait = StartCoroutine(FlowchartWait());
        }
        else
        {
            StartEncounter();
        }
    }

    IEnumerator FlowchartWait()
    {
        Debug.Log("Flowchart Wait Started");
        while (GameMaster.I.CurrentState is GMSInteract)
        {
            yield return null;
        }
        Debug.Log("Flowchart Wait Ended");
        StartEncounter();
    }

    void StartEncounter()
    {
        Debug.Log("Start Encounter");
        if(vc != null)
        {
            Debug.Log("Camera!");
            vc.enabled = true;
        }
        ToggleObstacles(true);
        wait = StartCoroutine(EncounterWait());
    }

    IEnumerator EncounterWait()
    {
        Debug.Log("Encounter Wait Started");
        while (encounterComplete == false)
        {
            yield return new WaitForSeconds(0.5f);
            CompleteEncounter();
        }
        Debug.Log("Encounter Wait Ended");
        yield return new WaitForSeconds(1f);
        if (vc != null)
        {
            vc.enabled = false;
        }
        yield return new WaitForSeconds(1f);
        EndEncounter();
    }

    void EndEncounter()
    {
        Debug.Log("End Encounter");
        if (endFlowchart != "" && FC.HasBlock(endFlowchart))
        {
            GM.LaunchConversation(FC, endFlowchart);
        }
        running = false;
        ToggleObstacles(false);
        gameObject.SetActive(false);
    }

    void CompleteEncounter()
    {
        bool allDead = true;
        foreach(Attackable z in encounterMembers)
        {
            //Debug.Log(z.gameObject.name + " isAlive: " + z.IsAlive);
            if(z.IsAlive)
            {
                allDead = false;
                break;
            }
        }
        encounterComplete = allDead;
    }

    void ToggleObstacles(bool set)
    {
        foreach (UnityEngine.AI.NavMeshObstacle o in obstacles)
        {
            o.gameObject.SetActive(set);
        }
    }
}
