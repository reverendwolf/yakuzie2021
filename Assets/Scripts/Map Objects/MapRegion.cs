﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MapRegion : MonoBehaviour
{
    const int MONSTERRESPAWNTIME = 300;

    public delegate void RegionEvent();
    public event RegionEvent OnRegionEnter;
    public event RegionEvent OnRegionLeave;

    [SerializeField] string regionName = "New Region";
    public string RegionName => regionName;

    List<Breakable> breakableRegistry = new List<Breakable>();
    [SerializeField] Fungus.Flowchart regionChart;
    public Fungus.Flowchart RegionChart => regionChart;
    [SerializeField] bool inside;
    public bool Inside => inside;
    [SerializeField] Color ambientColor;
    public Color AmbientColor => ambientColor;

    public void RegionEnter()
    {
        OnRegionEnter?.Invoke();
    }

    public void RegionLeave()
    {
        OnRegionLeave?.Invoke();
    }
}
