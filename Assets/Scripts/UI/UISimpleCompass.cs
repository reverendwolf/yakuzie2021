﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UISimpleCompass : MonoBehaviour
{
    RectTransform rt;
    // Start is called before the first frame update
    void Start()
    {
        rt = GetComponent<RectTransform>();
    }

    // Update is called once per frame
    void Update()
    {
        if(rt != null)
        {
            rt.rotation = Quaternion.Euler(0, 0, Camera.main.transform.rotation.eulerAngles.y);
        }
    }
}
