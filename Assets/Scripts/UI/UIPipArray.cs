﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIPipArray : MonoBehaviour
{
    [SerializeField] IntegerMonitor intMonitor;
    [SerializeField] Image[] pips = new Image[3];
    Image mainImage;
    [SerializeField] Color pipColor;
    [SerializeField] TMPro.TextMeshProUGUI text;
    private void OnEnable()
    {
        if(mainImage == null) mainImage = GetComponent<Image>();
        intMonitor.OnValueUpdate += UpdatePips;
    }

    private void OnDisable()
    {
        intMonitor.OnValueUpdate -= UpdatePips;
    }

    private void Start()
    {
        foreach (Image i in pips)
        {
            i.color = pipColor;
        }
    }

    void UpdatePips(int numPips)
    {
        if (numPips > pips.Length)
        {
            //Debug.Log("Doing " + numPips + " pips", this);
            foreach(Image i in pips)
            {
                i.gameObject.SetActive(false);
            }
            //emptyPip.gameObject.SetActive(false);
            //mainImage.enabled = true;
            pips[0].gameObject.SetActive(true);
            text?.gameObject.SetActive(true);
            text.text = numPips + "";
        }
        else if(numPips > 0)
        {
            text?.gameObject.SetActive(false);
            //mainImage.enabled = true;
            //emptyPip.gameObject.SetActive(false);
            for (int i = 0; i < pips.Length; i++)
            {
                if (i < numPips)
                {
                    pips[i].gameObject.SetActive(true);
                    //pips[i].color = pipColor;
                }
                else
                {
                    //pips[i].color = new Color(0.2f, 0.2f, 0.2f, 0.8f);
                    pips[i].gameObject.SetActive(false);
                }
            }
        }
        else
        {
            text?.gameObject.SetActive(false);
            //text.text = "0";
            //mainImage.enabled = false;
            foreach (Image i in pips)
            {
                //i.color = new Color(0.2f, 0.2f, 0.2f, 0.8f);
                i.gameObject.SetActive(false);
            }
            //emptyPip.gameObject.SetActive(true);
        }
    }
}
