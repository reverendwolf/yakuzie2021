﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UINumberMonitor : MonoBehaviour
{
    TMPro.TextMeshProUGUI text;
    float targetValue;
    float currentValue;
    [SerializeField] IntegerMonitor integerMonitor;
    // Start is called before the first frame update
    void Start()
    {
        text = GetComponent<TMPro.TextMeshProUGUI>();
    }

    // Update is called once per frame
    void Update()
    {
        currentValue = Mathf.MoveTowards(currentValue, targetValue, Time.deltaTime / 0.04f);
        text.text = (int)currentValue + "";
    }

    private void OnEnable()
    {
        if (integerMonitor != null) integerMonitor.OnValueUpdate += UpdateTarget;
    }

    private void OnDisable()
    {
        if (integerMonitor != null) integerMonitor.OnValueUpdate -= UpdateTarget;
    }

    void UpdateTarget(int newValue)
    {
        currentValue = targetValue;
        targetValue = newValue;
    }
}
