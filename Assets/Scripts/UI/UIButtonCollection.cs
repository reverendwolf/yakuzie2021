﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIButtonCollection : MonoBehaviour
{

    [SerializeField] Image[] buttons;
    [SerializeField] Color inactiveColor;
    public int Count => buttons.Length;

    // Start is called before the first frame update
    void Start()
    {
    
    }

    public void SelectButton(int select)
    {
        foreach(Image i in buttons)
        {
            i.color = inactiveColor;
        }

        buttons[select].color = Color.white;
    }
}
