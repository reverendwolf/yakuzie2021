﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIAttacksDisplay : MonoBehaviour
{
    [SerializeField] Image melee;
    [SerializeField] Image special;
    [SerializeField] Image dodge;
    [SerializeField] Image cast;
    [SerializeField] Image heavy;
    [SerializeField] Image heal;
    [SerializeField] Image ultimate;

    GameData GD => GameMaster.I.SaveData;

    private void OnEnable()
    {
        MapMaster.I.OnInitialize += Init;
    }

    private void Init()
    {
        MapMaster.I.OnInitialize -= Init;
        GameMaster.I.SaveData.OnDataUpdate += UpdateView;
    }

    private void OnDestroy()
    {
        GameMaster.I.SaveData.OnDataUpdate -= UpdateView;
    }

    void UpdateView()
    {
        //Debug.Log("Update View");
        melee.enabled = true;
        dodge.enabled = true;
        heal.enabled = true;

        special.enabled = GD.Skills[(int)Helper.UnlockList.Special + 2];
        cast.enabled = GD.Skills[(int)Helper.UnlockList.Cast + 2];
        heavy.enabled = GD.Skills[(int)Helper.UnlockList.Heavy + 2];
        ultimate.enabled = GD.Skills[(int)Helper.UnlockList.Ultimate + 2];
    }
}
