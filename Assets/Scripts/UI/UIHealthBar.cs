﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIHealthBar : MonoBehaviour
{
    int MaxHealth => GameMaster.I.SaveData.MaxHealth;

    [SerializeField] IntegerMonitor healthMonitor;
    [SerializeField] Image[] healthPips;


    private void OnEnable()
    {
        healthMonitor.OnValueUpdate += OnUpdateHealth;
    }

    private void OnDisable()
    {
        healthMonitor.OnValueUpdate -= OnUpdateHealth;
    }

    void OnUpdateHealth(int health)
    {
        UpdateView(health);
    }


    public void UpdateView(int currentHealth)
    {
        ShowMaxHealth();
        ShowCurrentHealth(currentHealth);
    }

    void ShowMaxHealth()
    {
        for (int i = 0; i < healthPips.Length; i++)
        {
            if(i < MaxHealth) healthPips[i].gameObject.SetActive(true);
            else healthPips[i].gameObject.SetActive(false);
        }
    }

    void ShowCurrentHealth(int current)
    {
        for (int i = 0; i < healthPips.Length; i++)
        {
            if (i < current) healthPips[i].color = Color.red;
            else healthPips[i].color = new Color(0.8f, 0.8f, 0.8f, 0.8f);
        }
    }

}
