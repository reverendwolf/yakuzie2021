﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIBarMonitor : MonoBehaviour
{
    [SerializeField] FloatMonitor floatMonitor;
    Slider slider;
    float targetValue;

    private void Awake()
    {
        slider = GetComponent<Slider>();
    }

    private void OnEnable()
    {
        if (floatMonitor != null) floatMonitor.OnValueUpdate += UpdateBar;
        else Debug.LogWarning("No monitor on " + gameObject.name, this);
    }

    private void OnDisable()
    {
        if (floatMonitor != null) floatMonitor.OnValueUpdate -= UpdateBar;
    }

    void UpdateBar(float newValue)
    {
        targetValue = newValue;
        //slider.value = newValue;
    }

    private void Update()
    {
        slider.value = Mathf.MoveTowards(slider.value, targetValue, Time.deltaTime);
    }
}
