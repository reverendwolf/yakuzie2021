﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class UIPop : MonoBehaviour
{
    [SerializeField] AnimationCurve popCurve;
    [SerializeField] Text textField;
    bool busy;
    public bool Busy => busy;
    Transform tracker;

    void Pop(Transform popTarget, float width, float height, float time, Color color)
    {
        tracker = popTarget;
        textField.transform.localPosition = Vector3.zero;
        
        Vector3 popPosition = Camera.main.WorldToScreenPoint(popTarget.position + Vector3.up * Random.value * 0.5f);
        transform.position = popPosition;
        textField.color = color;
        LeanTween.moveLocalY(textField.gameObject, height, time).setEase(popCurve);
        LeanTween.moveLocalX(textField.gameObject, Random.Range(-width,width), time).setEaseOutCubic();
        LeanTween.alphaText(textField.rectTransform, 0, time).setEaseInOutCubic().setDelay(time).setOnComplete(Finished);
        busy = true;
    }

    public void Pop(string word, Transform popTarget, Vector3 bounceSettings, Color color, bool scramble = true)
    {
        if (busy) return;
        if (scramble) StartCoroutine(ScrambleText(bounceSettings.z * 0.5f, word));
        else textField.text = word;
        Pop(popTarget, bounceSettings.x, bounceSettings.y, bounceSettings.z, color);
    }

    IEnumerator ScrambleText(float scrambleTime, string word)
    {
        while(scrambleTime >= 0f)
        {
            textField.text = Random.Range(10, 100).ToString();
            scrambleTime -= Time.deltaTime;
            yield return null;
        }
        textField.text = word;
    }

    void Finished()
    {
        busy = false;
    }

    private void Update()
    {
        if(busy)
        {
            transform.position = Camera.main.WorldToScreenPoint(tracker.position);
        }
    }
}
