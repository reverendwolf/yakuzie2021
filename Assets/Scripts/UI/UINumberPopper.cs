﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UINumberPopper : MonoBehaviour
{
    private static UINumberPopper _instance;
    public static UINumberPopper I
    {
        get
        {
            if (!_instance)
            {
                _instance = GameObject.FindObjectOfType(typeof(UINumberPopper)) as UINumberPopper;
            }

            if (!_instance)
            {
                Debug.LogError("There is no Type object!");
            }

            return _instance;
        }
    }

    public void OnEnable()
    {
        if (I != null && I != this)
            DestroyImmediate(this.gameObject);
    }

    [SerializeField] UIPop popObject;
    List<UIPop> pops = new List<UIPop>();
    [SerializeField] float bounceWidth;
    [SerializeField] float bounceHeight;
    [SerializeField] float bounceTime;
    Vector3 BounceSettings => new Vector3(bounceWidth, bounceHeight, bounceTime);
    UIPop NextPop()
    {
        for (int i = 0; i < pops.Count; i++)
        {
            if(pops[i].Busy == false)
            {
                return pops[i];
            }
        }

        pops.Add(Instantiate(popObject.gameObject, this.transform).GetComponent<UIPop>());
        return pops[pops.Count - 1];
    }

    public void PopNumber(int number, Transform target, Color color)
    {
        NextPop().Pop(number.ToString(), target, BounceSettings, color);
    }

    public void PopMessage(string message, Transform target, Color color)
    {
        NextPop().Pop(message, target, BounceSettings, color, false);
    }

    private void Update()
    {

    }
}
