﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraTriggerVolume : MonoBehaviour
{
    Transform child;
    // Start is called before the first frame update
    void Start()
    {
        child = transform.GetChild(0);
        child.gameObject.SetActive(false);
    }

    private void OnTriggerEnter(Collider other)
    {
        if(other.CompareTag("Player"))
        {
            child.gameObject.SetActive(true);
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            child.gameObject.SetActive(false);
        }
    }
}
