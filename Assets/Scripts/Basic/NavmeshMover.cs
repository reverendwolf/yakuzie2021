﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

[RequireComponent(typeof(NavMeshAgent))]
[RequireComponent(typeof(Rigidbody))]
public class NavmeshMover : MonoBehaviour
{
    public delegate void CallBack();
    public event CallBack OnPushComplete;

    NavMeshAgent agent;
    Transform t;
    Rigidbody rb;

    Coroutine walk;
    Coroutine push;

    [SerializeField] bool hasPath; //REMOVABLE
    [SerializeField] bool stopped; //REMOVABLE
    [SerializeField] bool walkingTo = false;
    public bool WalkingTo => walkingTo;

    [SerializeField] bool pushing = false;
    public bool Pushing => pushing;

    public float AgentSpeed => agent.speed;
    float normalSpeed;
    int normalPriority;
    public Vector3 Velocity => agent.velocity;
    // Start is called before the first frame update
    void Awake()
    {
        agent = GetComponent<NavMeshAgent>();
        agent.updateRotation = false;
        t = GetComponent<Transform>();
        rb = GetComponent<Rigidbody>();
        //agent.avoidancePriority = Random.Range(50, 100);
        rb.isKinematic = true;
        rb.useGravity = false;
        normalSpeed = agent.speed;
        normalPriority = agent.avoidancePriority;
    }

    private void OnDisable()
    {
        ForceStop();
    }

    private void Update()
    {
        if (walkingTo && agent.velocity != Vector3.zero ) SlerpFace(agent.velocity.ConvertToV2(), false);

        if(agent.isOnNavMesh)
        {
            hasPath = agent.hasPath;
            stopped = agent.isStopped;
        }
        
    }

    public void SlerpFace(Vector2 direction, bool slow = false)
    {
        Quaternion targetRotation = Quaternion.Euler(0, direction.FacingAngle(), 0);
        t.rotation = Quaternion.Lerp(t.rotation, targetRotation, agent.angularSpeed * Time.deltaTime * (slow ? 0.05f : 1f));
    }

    public void InstantFace(Vector2 direction)
    {
        t.rotation = Quaternion.Euler(0, direction.FacingAngle(), 0);
    }

    Coroutine turn;

    public void TurnToFace(Vector2 direction)
    {
        if(turn != null)
        {
            StopCoroutine(turn);
            turn = null;
        }

        turn = StartCoroutine(DoTurnToFace(direction));
    }

    IEnumerator DoTurnToFace(Vector2 direction)
    {
        float angle = transform.rotation.eulerAngles.y;
        float target = direction.FacingAngle();
        float timer = 0.6f;
        while (timer >= 0f)
        {
            SlerpFace(direction, false);
            angle = transform.rotation.eulerAngles.y;
            timer -= Time.deltaTime;
            yield return null;
        }
        Debug.Log("DoTurn Done");
    }

    public void WalkDirection(Vector2 inputDirection)
    {
        if (walkingTo) return;
        Debug.DrawRay(transform.position, new Vector3(inputDirection.x, transform.position.y, inputDirection.y), Color.blue, Time.deltaTime);
        agent.Move(new Vector3(inputDirection.x, 0, inputDirection.y).normalized * Time.deltaTime * agent.speed);
    }

    public void MoveDirection(Vector2 inputDirection, float speed = -1)
    {
        if (walkingTo) return;
        //Debug.Log("MDir: " + inputDirection);
        Debug.DrawRay(transform.position + Vector3.up * 0.1f, new Vector3(inputDirection.x, transform.position.y, inputDirection.y), Color.green, Time.deltaTime);
        agent.Move(new Vector3(inputDirection.x, 0, inputDirection.y).normalized * Time.deltaTime * (speed < 0 ? agent.speed : speed));
    }

    public void PushDirection(Vector2 direction, float distance, float time)
    {
        if (pushing) return;
        //Debug.Log("~Push Me~");
        ForceStop();
        //InstantFace(-direction);
        push = StartCoroutine(DoPush(direction, distance, time));
    }

    public void LungeDirection(Vector2 direction, float distance, float time)
    {
        if (pushing) return;
        ForceStop();
        InstantFace(direction);
        SetPriority(1);
        push = StartCoroutine(DoPush(direction, distance, time));
    }

    IEnumerator DoPush(Vector2 direction, float distance, float time)
    {
        pushing = true;
        float s = distance / time;
        float t = time;
        Debug.DrawRay(transform.position + Vector3.up * 0.5f, new Vector3(direction.x, transform.position.y, direction.y), Color.black, time);
        //Debug.Log("P: " + direction);
        while (t >= 0f)
        {
            //1 - (1 - x) * (1 - x);
            MoveDirection(direction, s * 1.25f * (t / time));
            //MoveDirection(direction, s * dodgeCurve.Evaluate(1 - t));
            t -= Time.deltaTime;
            yield return null;
        }
        OnPushComplete?.Invoke();
        pushing = false;
        ResetPriority();
    }

    public void MoveTo(Vector3 position)
    {
        //Debug.Log("Move To Recieved.");
        Debug.DrawLine(transform.position, position, Color.black, 1f);
        Debug.DrawLine(position, position + Vector3.up, Color.black, 1f);
        agent.isStopped = true;
        agent.ResetPath();
        walkingTo = false;

        if (!agent.SetDestination(position)) return;
        walk = StartCoroutine(MoveToWait());
    }

    public NavMeshPath CheckPathTo(Vector3 position)
    {
        NavMeshPath path = new NavMeshPath();
        agent.CalculatePath(position, path);
        return path;
    }

    public void TeleportTo(Vector3 position)
    {
        agent.Warp(position);
    }

    public void ForceStop()
    {
        if(agent.isActiveAndEnabled)
        {
            agent.isStopped = true;
            agent.ResetPath();
        }

        if (walkingTo) StopCoroutine(walk);
        walkingTo = false;
        //MoveDirection(Vector2.zero);
    }

    public void FullStop()
    {
        ForceStop();

        if (pushing) StopCoroutine(push);
        pushing = false;
    }

    public void Pause(bool pause)
    {
        agent.isStopped = pause;
    }

    public void SetSpeed(float newSpeed, bool permanent = false)
    {
        agent.speed = newSpeed;
        if (permanent) normalSpeed = newSpeed;
    }

    public void ResetSpeed()
    {
        agent.speed = normalSpeed;
    }

    public void SetPriority(int newPriority)
    {
        agent.avoidancePriority = newPriority;
    }

    public void ResetPriority()
    {
        agent.avoidancePriority = normalPriority;
    }

    IEnumerator MoveToWait()
    {
        walkingTo = true;
        //Debug.Log("Walking Started.");
        Debug.DrawRay(agent.destination, Vector3.up, Color.green, 15);
        while(Vector3.SqrMagnitude(transform.position - agent.destination) > 0.5f)
        {
            yield return null;
        }
        //Debug.Log("Done Walking");
        walkingTo = false;
        ForceStop();
    }

    
}
