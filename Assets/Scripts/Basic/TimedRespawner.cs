﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TimedRespawner : MonoBehaviour
{
    [SerializeField] float deathLifetime;
    [SerializeField] ParticleSystem respawnEffect;

    private void OnEnable()
    {
        CancelInvoke();
    }

    private void OnDisable()
    {
        Invoke("Respawn", deathLifetime + Random.value);
    }

    void Respawn()
    {
        if (gameObject.activeInHierarchy)
        {
            return;
        }
        else
        {
            gameObject.SetActive(true);
            if (respawnEffect != null) respawnEffect.Play();
            LeanTween.scale(gameObject, transform.localScale * 0.25f, 0.5f).setEasePunch();
        }
    }

}
