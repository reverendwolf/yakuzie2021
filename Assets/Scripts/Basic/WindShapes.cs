﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(SkinnedMeshRenderer))]
public class WindShapes : MonoBehaviour
{
    SkinnedMeshRenderer smr;
    Mesh m;
    [SerializeField] float windSpeed = 0.5f;
    [SerializeField] float windStrength = 1f;

    // Start is called before the first frame update
    void Awake()
    {
        smr = GetComponent<SkinnedMeshRenderer>();
        m = smr.sharedMesh;
    }

    // Update is called once per frame
    void Update()
    {
        float rx = (Mathf.PerlinNoise(transform.position.x, Time.time * windSpeed) - 0.5f) * (100f * windStrength);
        float ry = (Mathf.PerlinNoise(transform.position.y, Time.time * windSpeed) - 0.5f) * (100f * windStrength);

        smr.SetBlendShapeWeight(0, rx);
        smr.SetBlendShapeWeight(1, ry);
    }
}
