﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(AudioSource))]
public class RandomAudioCaller : MonoBehaviour
{
    AudioSource source;

    [SerializeField]
    AudioClip[] randomClips;

    [SerializeField]
    float minDelay = 5f;

    [SerializeField]
    float maxDelay = 15f;

    float rTime => Random.Range(minDelay, maxDelay);
    // Start is called before the first frame update
    void Start()
    {
        source = GetComponent<AudioSource>();
        StartCoroutine(RandomCaller());
    }

    void Play()
    {
        source.PlayOneShot(randomClips[Random.Range(0, randomClips.Length)]);
    }

    IEnumerator RandomCaller()
    {
        while(true)
        {
            yield return new WaitForSeconds(rTime);
            Play();
        }
    }
}
