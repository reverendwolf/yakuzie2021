﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RandomScale : MonoBehaviour
{
    [SerializeField] float minScale = 1;
    [SerializeField] float maxScale = 1.5f;
    // Start is called before the first frame update
    void Start()
    {
        transform.localScale = Vector3.one * Random.Range(minScale, maxScale);
    }

}
