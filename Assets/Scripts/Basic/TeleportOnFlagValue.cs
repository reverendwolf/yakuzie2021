﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TeleportOnFlagValue : FlagEvaluation
{
    [System.Serializable]
    public struct TeleportTouple
    {
        [Range(0,255)]
        public byte flagValue;
        public Transform targetPosition;
    }

    [SerializeField] TeleportTouple[] teleportList;
    Dictionary<byte, Vector3> cachedList;


    private void Start()
    {
        cachedList = new Dictionary<byte, Vector3>();
        for (int i = 0; i < teleportList.Length; i++)
        {
            if(teleportList[i].targetPosition != null)
                cachedList.Add(teleportList[i].flagValue, teleportList[i].targetPosition.position);
            else
                cachedList.Add(teleportList[i].flagValue, Vector3.zero);
        }
    }

    protected override void UpdateState(byte flagValue)
    {
        base.UpdateState(flagValue);
        if(cachedList.ContainsKey(flagValue))
        {
            transform.position = cachedList[flagValue];
        }
    }
}
