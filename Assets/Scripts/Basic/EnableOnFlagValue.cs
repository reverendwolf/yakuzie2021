﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnableOnFlagValue : FlagEvaluation
{
    protected override void UpdateState(byte flagValue)
    {
        base.UpdateState(flagValue);
        gameObject.SetActive(flagEval.Evaluate(flagValue));
    }
}
