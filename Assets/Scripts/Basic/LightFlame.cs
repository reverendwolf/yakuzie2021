﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LightFlame : MonoBehaviour
{
    Vector3 startPosition;
    [SerializeField] Gradient colorGradient;
    Light myLight;

    float scroll = 0f;
    float range;
    [SerializeField] float flickerIntensity;
    [SerializeField] float flickerSpeed;
    [SerializeField] float waverIntensity;
    [SerializeField] float waverSpeed;
    float RandomF => (Mathf.PerlinNoise(transform.position.x, Time.time * flickerSpeed) * flickerIntensity);
    //float RandomY => (Mathf.PerlinNoise(transform.position.y, Time.time * flickerSpeed) * flickerIntensity);
    //float RandomZ => (Mathf.PerlinNoise(transform.position.z, Time.time * flickerSpeed) * flickerIntensity);
    float RandomX => (Mathf.Sin(transform.position.x + (Time.time * waverSpeed)) * waverIntensity);
    float RandomY => (Mathf.Sin(transform.position.y + (Time.time * waverSpeed)) * waverIntensity);
    float RandomZ => (Mathf.Sin(transform.position.z + (Time.time * waverSpeed)) * waverIntensity);


    // Start is called before the first frame update
    void Start()
    {
        startPosition = transform.position;
        scroll = Random.value;
        myLight = GetComponent<Light>();
        range = myLight.range;
    }

    // Update is called once per frame
    void Update()
    {
        if(flickerSpeed > 0f)
        {
            myLight.color = colorGradient.Evaluate(scroll);
            scroll += Time.deltaTime * flickerSpeed;
            if (scroll > 1f) scroll = 0f;
        }

        if(flickerSpeed > 0f)
        {
            myLight.range = range + RandomF;
            transform.position = startPosition + new Vector3(RandomX, RandomY, RandomZ);
        }
    }
}
