﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Lifetime : MonoBehaviour
{
    [SerializeField] float lifetime = 3f;
    // Start is called before the first frame update
    void OnEnable()
    {
        Invoke("Die", lifetime);
    }

    private void OnDisable()
    {
        CancelInvoke();
    }

    void Die()
    {
        Debug.Log("I waited too long and died!", this);
        gameObject.SetActive(false);
    }
}
