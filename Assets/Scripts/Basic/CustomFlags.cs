﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CustomFlags : MonoBehaviour
{
    public enum Flag
    {
        UniqueEnemy //if dead, do not respawn on region load
    }

    [SerializeField] List<Flag> flags = new List<Flag>();
    public List<Flag> Flags => flags;
}
