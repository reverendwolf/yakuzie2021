﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class SimplePath
{
    [System.Serializable]
    private class PathPoint
    {

        [SerializeField] Vector3 point = Vector3.zero;
        [SerializeField] float randomDistance = 0;
        [SerializeField] float delayTime = 0;
        [SerializeField] bool randomDelay;

        public Vector3 Point => point;
        public float Distance => randomDistance;
        public Vector3 RandomPoint
        {
            get
            {
                Vector2 random = Random.insideUnitCircle * randomDistance;
                return new Vector3(point.x + random.x, point.y, point.z + random.y);
            }
        }
        public bool RandomDelay => randomDelay ? Random.value >= 0.5f : false;
        public float DelayTime => delayTime * Random.Range(0.75f, 1.25f);
    }

    [SerializeField] PathPoint[] pathPoints = new PathPoint[1];
    public int Length => pathPoints.Length;

    public Vector3 GetPoint(int index)
    {
        index = Mathf.Clamp(index, 0, pathPoints.Length);
        return pathPoints[index].Point;
    }

    public float GetDistance(int index)
    {
        index = Mathf.Clamp(index, 0, pathPoints.Length);
        return pathPoints[index].Distance;
    }

    public Vector3 GetRandomPoint(int index)
    {
        index = Mathf.Clamp(index, 0, pathPoints.Length);
        return pathPoints[index].RandomPoint;
    }

    public bool GetRandomWait(int index)
    {
        index = Mathf.Clamp(index, 0, pathPoints.Length);
        return pathPoints[index].RandomDelay;
    }

    public float GetWaitTime(int index)
    {
        index = Mathf.Clamp(index, 0, pathPoints.Length);
        return pathPoints[index].DelayTime;
    }
}
