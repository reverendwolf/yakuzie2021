﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class March : MonoBehaviour
{
    [SerializeField] Vector3 marchDirection = new Vector3(1,0,0);
    [SerializeField] float marchSpeed = 0.1f;

    // Update is called once per frame
    void Update()
    {
        transform.position += marchDirection.normalized * marchSpeed * Time.deltaTime;
    }
}
