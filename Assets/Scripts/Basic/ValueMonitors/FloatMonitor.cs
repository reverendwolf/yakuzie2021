﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu]
public class FloatMonitor : ScriptableObject
{
    public delegate void ValueUpdate(float newValue);

    public event ValueUpdate OnValueUpdate;

    [SerializeField] float currentValue;

    public void SetValue(float newValue)
    {
        currentValue = newValue;
        OnValueUpdate?.Invoke(newValue);
    }
}
