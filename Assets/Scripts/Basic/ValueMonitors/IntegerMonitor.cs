﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu]
public class IntegerMonitor : ScriptableObject
{
    public delegate void ValueUpdate(int newValue);

    public event ValueUpdate OnValueUpdate;

    [SerializeField] int currentValue;

    public int CurrentValue => currentValue;

    public void SetValue(int newValue)
    {
        currentValue = newValue;
        OnValueUpdate?.Invoke(newValue);
    }
}
