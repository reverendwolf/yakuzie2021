﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class FlagEval
{
    enum ValueCheck
    {
        EQ, GT, LT, NE
    }

    [SerializeField] GameFlag flag;
    [SerializeField] byte targetValue;
    [SerializeField] ValueCheck valueCheck;

    public GameFlag Flag => flag;

    public bool Evaluate(byte flagValue)
    {
        bool check = false;

        byte flagDiminished = (byte)((int)flagValue / 10);
        byte valueDiminished = (byte)((int)targetValue / 10);

        //Debug.Log("Checking Flag: " + flagDiminished + "/" + flagValue + " " + valueCheck.ToString() + " " + valueDiminished + "/" + targetValue);
        switch (valueCheck)
        {
            case ValueCheck.EQ:
                check = (flagDiminished == valueDiminished);
                break;
            case ValueCheck.NE:
                check = (flagDiminished != valueDiminished);
                break;
            case ValueCheck.GT:
                check = (flagDiminished > valueDiminished);
                break;
            case ValueCheck.LT:
                check = (flagDiminished < valueDiminished);
                break;
        }
        return check;
    }
}
