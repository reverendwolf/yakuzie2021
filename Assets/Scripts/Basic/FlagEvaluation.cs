﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class FlagEvaluation : MonoBehaviour
{
    public FlagEval flagEval;

    private void OnDestroy()
    {
        if (GameMaster.I != null) GameMaster.I.SaveData.OnDataUpdate -= DataUpdated;
    }

    // Start is called before the first frame update
    void Awake()
    {
        MapMaster.I.OnInitialize += Initialize;
    }

    void Initialize()
    {
        MapMaster.I.OnInitialize -= Initialize;
        MapMaster.I.GM.SaveData.OnDataUpdate += DataUpdated;
        DataUpdated();
    }

    void DataUpdated()
    {
        if (flagEval.Flag == null)
        {
            Debug.LogError("No GameFlag to check!", this);
            return;
        }

        byte flagValue = MapMaster.I.GM.SaveData.GetFlag(flagEval.Flag.name);

        UpdateState(flagValue);
    }

    protected virtual void UpdateState(byte flagValue)
    {

    }
}
