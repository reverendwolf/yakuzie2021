﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RandomChildSwitcher : MonoBehaviour
{
    [SerializeField] float waitTime = 10f;

    Coroutine switcher;
    WaitForSecondsRealtime waiter;
    WaitForSecondsRealtime smallWaiter;
    [SerializeField] CanvasGroup cg;

    List<int> grabBag = new List<int>();

    void OnEnable()
    {
        DisableAll();
        waiter = new WaitForSecondsRealtime(waitTime);
        smallWaiter = new WaitForSecondsRealtime(2f);
        switcher = StartCoroutine(DoSwitching());
    }

    void DisableAll()
    {
        for (int i = 0; i < transform.childCount; i++)
        {
            transform.GetChild(i).gameObject.SetActive(false);
        }
    }

    void FillBag()
    {
        for (int i = 0; i < transform.childCount - 1; i++)
        {
            grabBag.Add(i);
        }
    }

    int PullBag()
    {
        if (grabBag.Count == 0) FillBag();

        int r = Random.Range(0, grabBag.Count);

        int b = grabBag[r];

        grabBag.RemoveAt(r);

        return b;
    }

    IEnumerator DoSwitching()
    {
        yield return smallWaiter;

        int r = 0;
        while(true)
        {
            LeanTween.alphaCanvas(cg, 1, smallWaiter.waitTime);
            yield return smallWaiter;

            DisableAll();

            r = PullBag();

            transform.GetChild(r).gameObject.SetActive(true);
            yield return smallWaiter;

            LeanTween.alphaCanvas(cg, 0, smallWaiter.waitTime);
            yield return smallWaiter;

            

            yield return waiter;
        }
        
    }

}
