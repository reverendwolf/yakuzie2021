﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Projectile : MonoBehaviour
{
    [SerializeField] float speed;
    [SerializeField] GameObject deathSpawn;
    GameObject spawn;
    Hurtbox hb;
    [SerializeField] bool destroyOnAttackable;
    bool pause;
    private void Awake()
    {
        GameMaster.OnPauseGame += OnPause;
    }

    // Update is called once per frame
    void Update()
    {
        if(!pause) transform.position += transform.forward * speed * Time.deltaTime;
    }

    private void OnEnable()
    {
        hb = GetComponent<Hurtbox>();
        if (hb != null)
        {
            hb.OnHurtSomething += HurtSomething;
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.layer == LayerMask.NameToLayer("Level") || other.gameObject.layer == LayerMask.NameToLayer("Default"))
        {
            Debug.Log("I hit " + other.gameObject.name + " and died!", this);
            gameObject.SetActive(false);
        }
    }

    private void OnDisable()
    {
        if (hb != null)
        {
            hb.OnHurtSomething -= HurtSomething;
        }

        if (deathSpawn != null)
        {
            if (spawn != null)
            {
                spawn.SetActive(false);
                spawn.transform.position = transform.position;
                spawn.transform.rotation = transform.rotation;
                spawn.SetActive(true);
            }
            else
            {
                spawn = Instantiate(deathSpawn, transform.position, transform.rotation, null);
            }
        }

    }

    void OnPause(bool pause)
    {
        hb.enabled = !pause;
        this.pause = pause;
    }

    void HurtSomething(Attackable attackable)
    {
        if (destroyOnAttackable == false) return;

        gameObject.SetActive(false);
    }
}
