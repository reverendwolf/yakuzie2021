﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class Helper
{
    public enum UnlockList
    {
        Special,
        Heavy,
        Cast,
        Ultimate
    }

    public enum SkillStatus
    {
        Locked,
        Obtained,
        Purchased
    }

    public enum Cardinal
    {
        North,
        East,
        South,
        West
    }

    public enum Comparison
    {
        GT, LT, EQ
    }

    public enum Operation
    {
        SET, ADD, SUB
    }

    public enum Dice
    {
        d1 = 1,
        d4 = 4,
        d6 = 6,
        d8 = 8,
        d12 = 12,
        d20 = 20,
        d100 = 100
    }

    public static int RollDice(Dice d)
    {
        return Random.Range(1, (int)d);
    }

    public static Vector2 RelativeDirection(this Camera cam, Vector2 direction)
    {
        if (direction == Vector2.zero) return direction;
        float facing = cam.transform.rotation.eulerAngles.y;
        Vector3 turnedInput = Quaternion.Euler(0, facing, 0) * new Vector3(direction.x, 0, direction.y);
        return new Vector2(turnedInput.x, turnedInput.z);
    }

    public static Vector2 ConvertToV2(this Vector3 v3)
    {
        return new Vector2(v3.x, v3.z);
    }

    public static Vector3 Flatten(this Vector3 v3)
    {
        return new Vector3(v3.x, 0, v3.z);
    }

    public static float FacingAngle(this Vector2 vec)
    {
        return Mathf.Atan2(vec.x, vec.y) * Mathf.Rad2Deg;
    }

    public static void ResetAllTriggers(this Animator anim)
    {
        foreach(AnimatorControllerParameter acp in anim.parameters)
        {
            if(acp.type == AnimatorControllerParameterType.Trigger)
            {
                anim.ResetTrigger(acp.nameHash);
            }
        }
    }
}
