﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class HKAttackDeck
{
    [SerializeField] HKAttackVolume normal;
    [SerializeField] HKAttackVolume normalCharged;

    [SerializeField] HKAttackBase heavy;

    [SerializeField] HKAttackBase special;

    [SerializeField] HKAttackSpawner spell;

    [SerializeField] HKAttackBase ultimate;

    public HKAttackVolume GetMelee(bool charged)
    {
        return charged ? normalCharged : normal;
    }

    public HKAttackBase GetHeavy()
    {
        return heavy;
    } 

    public HKAttackBase GetSpecial()
    {
        return special;
    }

    public HKAttackSpawner GetSpell()
    {
        return spell;
    }
}
