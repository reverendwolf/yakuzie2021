﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Rigidbody))]
public class Pickup : MonoBehaviour
{
    const int FORCEMULTIPLIER = 3;

    public enum PickupType
    {
        Health,
        Overhealth,
        Energy,
        Copper,
        Silver,
        Gold,
        Platinum
    }
    [SerializeField] PickupType pickupType;
    [SerializeField] Sprite[] pickupGraphics;
    [SerializeField] Color[] pickupColors;
    [SerializeField] SpriteRenderer graphic;
    Rigidbody rb;
    // Start is called before the first frame update
    void Start()
    {
        rb = GetComponent<Rigidbody>();
        SetPickup(pickupType);
    }

    public void SetPickup(PickupType type)
    {
        pickupType = type;
        graphic.sprite = pickupGraphics[(int)pickupType];
        graphic.color = pickupColors[(int)pickupType];
    }

    public void Pop()
    {
        if(rb == null) rb = GetComponent<Rigidbody>();
        //Vector2 random = Random.insideUnitCircle.normalized;
        //Vector3 force = new Vector3(random.x, 5, random.y) * FORCEMULTIPLIER;
        Vector3 force = Vector3.up;
        rb.AddForce(Vector3.up * FORCEMULTIPLIER * 5, ForceMode.Impulse);
        rb.detectCollisions = true;
        //rb.AddTorque(Random.insideUnitSphere.normalized * FORCEMULTIPLIER, ForceMode.Impulse);
    }

    private void OnTriggerEnter(Collider other)
    {
        if(other.CompareTag("Player"))
        {
            //Debug.Log("Enter Trigger: " + other + " :" + Time.time);
            //Debug.Log("Pickup!");
            GameMaster.I.HandlePickup(pickupType);
            PopAbovePlayer();
        }
    }

    void PopAbovePlayer()
    {
        if (rb == null) rb = GetComponent<Rigidbody>();
        transform.parent = GameMaster.I.PlayerTransform;
        rb.velocity = Vector3.zero;
        rb.detectCollisions = false;
        transform.localPosition = Vector3.zero;
        transform.localPosition = Vector3.up * 2;
        LeanTween.moveLocalY(this.gameObject, 3f, 0.25f).setEaseOutBounce().setOnComplete(Die);
    }

    void Die()
    {
        transform.SetParent(null);
        gameObject.SetActive(false);
    }
}
