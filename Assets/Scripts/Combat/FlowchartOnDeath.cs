﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Attackable))]
public class FlowchartOnDeath : MonoBehaviour
{
    Attackable a;
    [SerializeField] Fungus.Flowchart flowchart;
    [SerializeField] string slug;
    void OnEnable()
    {
        if (a == null) a = GetComponent<Attackable>();
        a.OnDeathEvent += OnDeath;
    }

    private void OnDisable()
    {
        a.OnDeathEvent -= OnDeath;
    }

    void OnDeath()
    {
        GameMaster.I.LaunchConversation(flowchart, slug);
    }
}
