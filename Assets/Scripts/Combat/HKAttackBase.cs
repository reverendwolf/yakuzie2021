﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public abstract class HKAttackBase : MonoBehaviour
{
    
    public enum DamageTag
    {
        Raw,
        Melee,
        Magic
    }
    [System.Serializable]
    public class AttackLevel
    {
        public enum DamageBonus
        {
            melee,
            magic,
            raw
        }

        public int cost;
        public int baseDamage;
        public DamageBonus damageBonus;
        public bool knockBack;
        public DamageType damageType;
        public ParticleSystem useEffect;

        [SerializeField] AudioClip[] sounds;
        public AudioClip Sound => sounds[Random.Range(0, sounds.Length)];
        public bool HasSound => sounds.Length > 0;
        public int DamageValue // => baseDamage + (damageBonus == DamageBonus.melee ? 0 : damageBonus == DamageBonus.magic ? 0 : 0);
        {
            get
            {
                int bonus = 0;
                if (damageBonus == DamageBonus.melee)
                {
                    bonus = GameMaster.I.SaveData.MeleeDamage;
                }
                return baseDamage + bonus;
            }
        }
    }
    private HKPlayer player;
    protected DamageTag damageTag;
    public DamageTag Tag => damageTag;
    [SerializeField] protected AttackLevel attackLevel;
    public AttackLevel AL => attackLevel;
    protected AttackData AD => new AttackData(AL.DamageValue, AL.damageType, AL.knockBack, this.transform.position, this.transform.root);
    AudioSource audioSource;

    private void Awake()
    {
        audioSource = GetComponent<AudioSource>();
    }

    public virtual void Hit()
    {
        if (attackLevel.useEffect != null) attackLevel.useEffect.Play();
        if(audioSource != null && attackLevel.HasSound)
        {
            audioSource.pitch = Random.Range(0.85f, 1.15f);
            audioSource.PlayOneShot(attackLevel.Sound);
        }
    }
}
