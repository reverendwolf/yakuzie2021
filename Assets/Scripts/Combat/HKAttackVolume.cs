﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Hurtbox))]
public class HKAttackVolume : HKAttackBase
{
    Hurtbox hb;
    int enemiesHit;
    int spReward;
    public int EnemiesHit => enemiesHit;
    public int SPReward => spReward;
    [SerializeField] float activationTime = 0.2f;
    public Hurtbox Hurtbox => hb;

    private void Start()
    {
        hb = GetComponent<Hurtbox>();
        hb.SetDamageInfo(AL);
        hb.Deactivate();
    }

    public override void Hit()
    {
        enemiesHit = 0;
        spReward = 0;
        base.Hit();
        hb.SetDamageInfo(AL);
        hb.ActivateTime(activationTime);

        /*foreach(Attackable a in sensor.Attackables)
        {
            if (a.HasIFrames == false)
            {
                enemiesHit++;
                if (a.SPReward) spReward++;
            }
                
            a.Damage(AD);
        }*/
    }
}
