﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HKPlayerVFX : MonoBehaviour
{
    [SerializeField] ParticleSystem footSmoke;
    [SerializeField] ParticleSystem surgeCharge;
    [SerializeField] ParticleSystem surgeHit;
    [SerializeField] ParticleSystem attackCharge;
    [SerializeField] ParticleSystem attackChargeReady;
    [SerializeField] ParticleSystem dodge;
    [SerializeField] HKAfterImage afterImage;

    public HKAfterImage AfterImage => afterImage;

    [SerializeField] AudioClip[] steps;
    [SerializeField] AudioClip oof;

    public void Step()
    {
        footSmoke?.Play();
        if (steps.Length > 0 && footSmoke?.GetComponent<AudioSource>())
        {
            footSmoke.GetComponent<AudioSource>().pitch = Random.Range(0.85f, 1f);
            footSmoke.GetComponent<AudioSource>().PlayOneShot(steps[Random.Range(0, steps.Length)]);
        }
    }

    public void ChargeReady()
    {
        attackChargeReady?.Play();
        if(attackChargeReady.GetComponent<AudioSource>())
        {
            attackChargeReady.GetComponent<AudioSource>().Play();
        }
    }

    public void Dodge()
    {
        footSmoke?.Play();
        dodge?.Play();
        if(dodge.GetComponent<AudioSource>())
        {
            dodge.GetComponent<AudioSource>().pitch = Random.Range(0.85f, 1f);
            dodge.GetComponent<AudioSource>().Play();
        }
    }

    public void SurgeHit()
    {
        surgeHit.Play();
        if (surgeHit.GetComponent<AudioSource>())
        {
            surgeHit.GetComponent<AudioSource>().pitch = Random.Range(0.85f, 1f);
            surgeHit.GetComponent<AudioSource>().Play();
        }
    }

    public void SurgeCharge(bool play)
    {
        if(play)
        {
            if(surgeCharge.isStopped)
            {
                surgeCharge.Play();
                if (surgeCharge.GetComponent<AudioSource>())
                {
                    //surgeCharge.GetComponent<AudioSource>().pitch = Random.Range(0.85f, 1f);
                    surgeCharge.GetComponent<AudioSource>().Play();
                }
            }

        }
        else
        {
            surgeCharge.Stop();
            if (surgeCharge.GetComponent<AudioSource>())
            {
                //surgeCharge.GetComponent<AudioSource>().pitch = Random.Range(0.85f, 1f);
                surgeCharge.GetComponent<AudioSource>().Stop();
            }
        }
    }

    public void AttackCharge(bool play)
    {
        if(play)
        {
            if(attackCharge.isStopped)
            {
                attackCharge.Play();
                if (attackCharge.GetComponent<AudioSource>())
                {
                    //surgeCharge.GetComponent<AudioSource>().pitch = Random.Range(0.85f, 1f);
                    attackCharge.GetComponent<AudioSource>().Play();
                }
            }

        }
        else
        {
            attackCharge.Stop();
            if (attackCharge.GetComponent<AudioSource>())
            {
                //surgeCharge.GetComponent<AudioSource>().pitch = Random.Range(0.85f, 1f);
                attackCharge.GetComponent<AudioSource>().Stop();
            }
        }
    }

}
