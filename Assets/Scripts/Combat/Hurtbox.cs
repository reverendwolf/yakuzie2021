﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
public class Hurtbox : MonoBehaviour
{
    public delegate void HurtboxEvent(Attackable attackable);
    public event HurtboxEvent OnHurtSomething;
    public event HurtboxEvent OnHurtMonster;
    public event HurtboxEvent OnHurtPlayer;

    [SerializeField] bool hurtPlayer;
    [SerializeField] bool hurtMonsters;
    [SerializeField] bool hurtObjects;

    [SerializeField] int damageValue;
    [SerializeField] DamageType damageType;
    [SerializeField] bool knockback;
    [SerializeField] float damageCooldown = 0.5f;
    Transform owner;
    [SerializeField] bool active = true;
    //keep track of attackables we hit recently bu marking them with Time.time + damageCooldown
    //this is separate from iframes, so we can have things that have big effects with long cooldowns
    Dictionary<Attackable, float> recentlyAttacked = new Dictionary<Attackable, float>();
    [SerializeField] ParticleSystem hitSpark;

    private void OnTriggerStay(Collider other)
    {
        if (active == false) return;
        //Debug.Log(other.gameObject.name + ":  " + other.tag.ToString());
        if ((hurtPlayer && other.CompareTag("Player")) || (hurtMonsters && other.CompareTag("Monster")) || ( hurtObjects && other.CompareTag("Object")))
        {
            Attackable a = other.GetComponent<Attackable>();

            Debug.Log("I found " + other.gameObject.name);

            if (recentlyAttacked.ContainsKey(a))
            {
                //recently attacked
                if(Time.time > recentlyAttacked[a])
                {
                    //remove it if the current time exceeds the time in the dictionary
                    recentlyAttacked.Remove(a);
                }
            }

            if(recentlyAttacked.ContainsKey(a) == false)
            {
                AttackData ad = new AttackData(damageValue, damageType, knockback, transform.position, owner == null ? transform.root : owner);
                //Debug.Log("I found " + other.gameObject.name);

                if (a != null && a.HasIFrames == false)
                {
                    a.Damage(ad);
                    OnHurtSomething?.Invoke(a);
                    hitSpark?.Play();
                    if (other.CompareTag("Player")) OnHurtPlayer?.Invoke(a);
                    if (other.CompareTag("Monster")) OnHurtMonster?.Invoke(a);
                    recentlyAttacked.Add(a, Time.time + damageCooldown);
                }
            }
            
        }
    }

    public void SetDamageInfo(HKAttackBase.AttackLevel ad, Transform owner = null)
    {
        damageValue = ad.DamageValue;
        damageType = ad.damageType;
        knockback = ad.knockBack;
        if(owner!= null) this.owner = owner;
    }

    public void ActivateTime(float time)
    {
        active = true;
        Invoke("Deactivate", time);
        recentlyAttacked.Clear();
    }

    public void Deactivate()
    {
        CancelInvoke("Deactivate");
        active = false;
    }
}
