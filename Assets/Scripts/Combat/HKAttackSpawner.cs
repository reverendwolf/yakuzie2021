﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HKAttackSpawner : HKAttackBase
{
    public enum SpawnTimer
    {
        OnBegin, OnHit
    }

    [SerializeField] SpawnTimer spawnTimer;
    [SerializeField] Hurtbox spawnObject;
    Vector3 SpawnPoint => spawnPoint != Vector3.zero ? spawnPoint : transform.position;

    List<Hurtbox> pool = new List<Hurtbox>();

    Vector3 spawnPoint;

    private void OnDrawGizmosSelected()
    {
        Gizmos.color = Color.white;
        Gizmos.DrawCube(SpawnPoint, Vector3.one * 0.15f);
    }

    Hurtbox GetNext()
    {
        Hurtbox _return = null;
        for (int i = 0; i < pool.Count; i++)
        {
            if(pool[i].isActiveAndEnabled == false)
            {
                _return = pool[i];
                _return.transform.position = SpawnPoint;
                _return.transform.rotation = transform.rotation;
                _return.gameObject.SetActive(true);
                break;
            }
        }

        if(_return == null)
        {
            pool.Add(Instantiate(spawnObject.gameObject, SpawnPoint, transform.rotation, transform.root).GetComponent<Hurtbox>());
            _return = pool[pool.Count - 1];
        }

        return _return;
    }

    public void SetSpawnPoint(Vector3 position)
    {
        spawnPoint = position;
    }

    public override void Hit()
    {
        base.Hit();
        Spawn();
        /*
        if (spawnTimer == SpawnTimer.OnHit)
        {
            Spawn();
        }*/
    }

    void Spawn()
    {
        //Debug.Log("Hurtbox spawn");
        Hurtbox hb = GetNext();
        //Debug.Log("Hurtbox set");
        hb.SetDamageInfo(AL, transform.parent);
        //Debug.Log("Hurtbox null parent");
        hb.transform.parent = null;
    }
}
