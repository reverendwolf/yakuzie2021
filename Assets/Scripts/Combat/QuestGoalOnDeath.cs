﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Attackable))]
public class QuestGoalOnDeath : MonoBehaviour
{
    public enum Operation
    {
        Set, Add
    }
    Attackable a;
    [SerializeField] MissionGoal goal;
    [SerializeField] Operation operation;
    [SerializeField] int value;
    // Start is called before the first frame update
    void Start()
    {
        a = GetComponent<Attackable>();
        a.OnDeathEvent += OnDeath;
    }
    
    void OnDeath()
    {
        switch(operation)
        {
            case Operation.Add:
                goal.AddProgress(value);
                break;
            case Operation.Set:
                goal.SetProgress(value);
                break;
        }
    }

    private void OnDestroy()
    {
        a.OnDeathEvent -= OnDeath;
    }
}
