﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Attackable))]
public class FlowchartVariableOnDeath : MonoBehaviour
{
    public enum Operation
    {
        Set,
        Add
    }

    Attackable a;
    [SerializeField] Fungus.Flowchart flowchart;
    [SerializeField] string variableID;
    [SerializeField] int value;
    [SerializeField] Operation operation;
    void OnEnable()
    {
        if (a == null) a = GetComponent<Attackable>();
        a.OnDeathEvent += OnDeath;
    }

    private void OnDisable()
    {
        a.OnDeathEvent -= OnDeath;
    }

    void OnDeath()
    {
        //GameMaster.I.LaunchConversation(flowchart, slug);
        switch(operation)
        {
            case Operation.Add:
                flowchart.SetIntegerVariable(variableID, flowchart.GetIntegerVariable(variableID) + value);
                break;
            case Operation.Set:
                flowchart.SetIntegerVariable(variableID, value);
                break;
        }
    }
}
