﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu()]
public class HKSkillTree : ScriptableObject
{
    public enum Move
    {
        ChargeSpecial,
        MeleeSkill,
        SpecialSkill,
        Spell1,
        Spell2,
        Ultimate
    }

    [System.Serializable]
    public class TreasureUnlocks
    {
        public bool chargeSpecial;
        public bool meleeSkill;
        public bool specialSkill;
        public bool spell1;
        public bool spell2;
        public bool ultimate;

        public bool[] AllUnlocks => new bool[] { true, true, chargeSpecial, meleeSkill, specialSkill, spell1, spell2, ultimate };
        public bool[] AllAbilities => new bool[] { true, true, true, true, chargeSpecial, meleeSkill, specialSkill, spell1, spell2, ultimate };
        public bool HasSpells => (spell1 || spell2);

    }

    [System.Serializable]
    public class Treasure
    {
        [SerializeField] string treasureName;
        [SerializeField] [TextArea] string desc;
        [SerializeField] string moveName;

        public string Name => treasureName;
        public string Desc => desc;
        public string MoveName => moveName;
    }
    [SerializeField] string characterName;
    [SerializeField] string characterClass;
    [SerializeField] Treasure meleeWeapon;
    [SerializeField] string meleeCharge;
    [SerializeField] string special;
    [SerializeField] Treasure focus;
    [SerializeField] Treasure chargeSpecial;
    [SerializeField] Treasure meleeSkill;
    [SerializeField] Treasure specialSkill;
    [SerializeField] Treasure spell1;
    [SerializeField] Treasure spell2;
    [SerializeField] Treasure ultimate;
    public string[] Moves => new string[] { meleeWeapon.MoveName, meleeCharge, special, focus.MoveName, chargeSpecial.MoveName, meleeSkill.MoveName, specialSkill.MoveName, spell1.MoveName, spell2.MoveName, ultimate.MoveName};
    public Treasure[] AllTreasures => new Treasure[] { meleeWeapon, focus, chargeSpecial, meleeSkill, specialSkill, spell1, spell2, ultimate };
    public string[] CharacterBio => new string[] { characterName, characterClass };
}
