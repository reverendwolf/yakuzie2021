﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Attackable))]
public class Breakable : MonoBehaviour
{
    Attackable attackable;
    private void OnEnable()
    {
        attackable = GetComponent<Attackable>();
    }

    void DieAndDrop()
    {
        attackable.EvaluateLife();
    }

    // Start is called before the first frame update
    void Start()
    {
        attackable.OnAttackEvent += DieAndDrop;
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
