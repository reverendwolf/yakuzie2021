﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Attackable))]
public class PickupDeathDrop : MonoBehaviour
{
    [SerializeField] [Range(0, 20)] int dropChance = 0;
    [SerializeField] [Range(1, 12)] int maxDrops = 1;

    [SerializeField] Pickup.PickupType[] options;

    public void OnEnable()
    {
        GetComponent<Attackable>().OnDeathEvent += Drop;
    }

    public void OnDisable()
    {
        GetComponent<Attackable>().OnDeathEvent -= Drop;
    }

    void Drop()
    {
        bool drop = Helper.RollDice(Helper.Dice.d20) < dropChance;
        if(drop)
        {
            int drops = Random.Range(1, maxDrops);
            for (int i = 0; i < drops; i++)
            {
                Vector3 dropPosition = transform.position + Vector3.up;
                GameMaster.I.SpawnPickup(options[Random.Range(0, options.Length)], dropPosition);
            }
        }
    }
}
