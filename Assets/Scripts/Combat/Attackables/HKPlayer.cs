﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(NavmeshMover))]
[RequireComponent(typeof(Attackable))]
public class HKPlayer : MonoBehaviour, IAnimEvents
{
    const int SURGECOST = 3;
    public enum AttackClass
    {
        Normal,
        Special,
        Heavy,
        Spell
    }

    Attackable attackable;
    NavmeshMover mover;
    Animator anim;
    [SerializeField] MapObjectSensor interactionSensor;
    [SerializeField] CombatSensor combatSensor;

    public Attackable Attackable => attackable;
    public NavmeshMover Mover => mover;

    [SerializeField] float runMultiplier = 1.3f;

    AttackClass attackSelected;
    HKAttackBase hkaSel;

    bool moveLock;
    bool actionLock;
    bool inputMove;
    bool Surging => NormalizedFocusCharge > 0f;
    bool chargeReady;
    float localCharge;
    [SerializeField] float focusCharge;
    bool hitStop;
    bool Pathfinding => mover.WalkingTo;
    bool Pushed => mover.Pushing;
    public bool ActionLocked => actionLock;
    public bool MoveLocked => moveLock;
    bool running;
    float moveTime;

    float idleTime;

    int localSP;
    int LocalSP
    {
        get { return localSP; }
        set { localSP = Mathf.Clamp(value, 0, GameMaster.I.SaveData.MaxBar); if (spBar != null) spBar.SetValue((float)localSP/(float)GameMaster.I.SaveData.MaxBar); }
    }

    int localArmor;
    int LocalArmor
    {
        get { return localArmor; }
        set { localArmor = Mathf.Max(value, 0); if (armorPips != null) armorPips.SetValue(localArmor); }
    }

    //float nCharge;
    float NormalizedCharge
    {
        get { return Mathf.Clamp(localCharge, 0f, 1f); }
    }

    float NormalizedFocusCharge
    {
        get { return Mathf.Clamp(focusCharge, 0f, 1f); }
    }

    [SerializeField] FloatMonitor spBar;
    [SerializeField] IntegerMonitor armorPips;
    [SerializeField] float dashDistance = 3.5f;
    [SerializeField] float dashTime = 0.45f;
    [SerializeField] HKAttackDeck attackDeck;
    public HKAttackDeck AttackDeck => attackDeck;
    public bool CanCast => (LocalSP >= attackDeck.GetSpell().AL.cost);
    public bool CanSpecial => (LocalSP >= attackDeck.GetSpecial().AL.cost);

    HKPlayerVFX vfx;

    private void OnEnable()
    {
        attackable = GetComponent<Attackable>();
        mover = GetComponent<NavmeshMover>();
        anim = transform.GetChild(0).GetComponent<Animator>();
        vfx = GetComponent<HKPlayerVFX>();

        attackable.IncomingAttack += ProcessIncomingAttack;
        attackable.OnAttack += OnIncomingAttack;
        //GameMaster.I.DataMaster.OnGameDataUpdate += UpdatePlayerStats;
    }

    private void OnDisable()
    {
        attackable.IncomingAttack -= ProcessIncomingAttack;
        attackable.OnAttack -= OnIncomingAttack;
        //GameMaster.I.DataMaster.OnGameDataUpdate -= UpdatePlayerStats;
    }

    // Start is called before the first frame update
    void Start()
    {
        GMSMissionStart.OnMissionInit += InitializePlayer;
    }

    void DebugResetSP()
    {
        LocalSP = GameMaster.I.SaveData.MaxBar;
    }

    void InitializePlayer()
    {
        GMSMissionStart.OnMissionInit -= InitializePlayer;
        GameMaster.I.RegisterPlayerObject(this.gameObject);
        UpdatePlayerStats();
        LocalArmor = 2;
        attackDeck.GetMelee(false).Hurtbox.OnHurtMonster += OnMeleeHitMonster;
    }

    void UpdatePlayerStats()
    {
        Debug.Log("Update Stats!");
        LocalSP = GameMaster.I.SaveData.MaxBar;
        attackable.UpdateMaxHP(GameMaster.I.SaveData.MaxHealth);
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Semicolon)) DebugResetSP();
        if (Input.GetKeyDown(KeyCode.Quote)) GameMaster.I.GrantExperience(10);

        anim.SetFloat("InputMove", Pathfinding ? 1f : inputMove ? 1f : 0f);

        /*if(running == false && combatSensor.MonsterCount == 0 && moveTime > runTime)
        {
            StartRunning();
        }*/

        /*if (running == true && combatSensor.MonsterCount > 0)
        {
            StopRunning();
        }*/

    }

    void StartRunning()
    {
        if(running == false)
        {
            //Debug.Log("Start Running");
            running = true;
            vfx.Step();
            vfx.Step();
            anim.speed = runMultiplier;
            mover.SetSpeed(mover.AgentSpeed * runMultiplier);
        }
    }

    void StopRunning()
    {
        if(running)
        {
            //Debug.Log("Stop Running");
            vfx.Step();
            running = false;
            anim.speed = 1;
            mover.ResetSpeed();
        }
        moveTime = 0f;
    }

    public void MovementLock(bool input)
    {
        moveLock = input;
        //if (moveLock == false) ResetCombat();
    }

    public void ActionLock(bool input)
    {
        //Debug.Log("Action " + (input ? "Locked" : "Unlocked"));
        actionLock = input;
    }

    public void FullLock(bool input)
    {
        MovementLock(input);
        ActionLock(input);
    }

    public void InputMove(Vector2 direction)
    {
        inputMove = false;
        if (direction == Vector2.zero)
        {
            idleTime += Time.deltaTime;
            StopRunning();
            return;
        }
        //if (hitStop || Pathfinding || Pushed || NormalizedFocusCharge > 0f)
        if (hitStop || Pathfinding || NormalizedFocusCharge > 0f)
        {
            StopRunning();
            return;
        }

        if (!moveLock)
        {
            inputMove = true;
            if (idleTime > 0.5f)
            {
                vfx.Step();
            }
            idleTime = 0f;
            moveTime += Time.deltaTime;
            mover.WalkDirection(direction);
        }

        mover.SlerpFace(direction, moveLock);
    }

    public void InputDodge(Vector2 direction)
    {
        if (actionLock) return;
        if (hitStop || Pathfinding || Pushed) return;

        ResetCombat();
        anim.SetTrigger("InputDodge");

        //AfterImage();
        vfx.Dodge();
        mover.LungeDirection(direction, dashDistance, dashTime);
        attackable.IFrames(0.5f);
        StartRunning();
    }

    public void InputNormal()
    {
        if (actionLock || Surging) return;
        chargeReady = false;
        attackSelected = AttackClass.Normal;
        if (NormalizedCharge >= 1f)
        {
            anim.SetTrigger("InputNormalCharged");
            hkaSel = attackDeck.GetMelee(true);
            attackable.IFrames(1.5f);
        }
        else if(NormalizedCharge == 0f)
        {
            anim.SetTrigger("InputNormal");
            hkaSel = attackDeck.GetMelee(false);
        }
        StopRunning();
        mover.ForceStop();
        //hkaSel.SetLevel(0);
    }

    public void InputSpecial()
    {
        Debug.Log("Special Input Start");
        attackSelected = AttackClass.Special;

        hkaSel = attackDeck.GetSpecial();

        if (LocalSP >= hkaSel.AL.cost)
        {
            Debug.Log("Special Cost Check");
            mover.ForceStop();
            anim.SetTrigger("InputSpecial");
        }
        InputCastPrepare(false);
    }

    public void InputHeavy()
    {
        if (actionLock) return;
        attackSelected = AttackClass.Heavy;
        hkaSel = attackDeck.GetHeavy();
        //hkaSel.SetLevel(0);

        if(LocalSP >= hkaSel.AL.cost)
        {
            mover.ForceStop();
            anim.SetTrigger("InputHeavy");
            attackable.IFrames(0.65f);
        }
    }

    public void InputCast(Vector3 spellPosition)
    {
        //if (actionLock) return;
        //Debug.Log("HK Spell: " + spellID + " at " + spellPosition.ToString());
        attackSelected = AttackClass.Spell;
        HKAttackSpawner spell = attackDeck.GetSpell();
        spell.SetSpawnPoint(spellPosition);
        hkaSel = spell;
        //hkaSel.SetLevel(0);

        if(LocalSP >= hkaSel.AL.cost)
        {
            mover.ForceStop();
            anim.SetTrigger("InputCast");
        }
        InputCastPrepare(false);
    }

    public void InputThrow()
    {

    }

    public void InputCastPrepare(bool input)
    {
        if (actionLock && input == true) return;
        anim.SetBool("InputCastPrepare", input);
    }

    public void InputInteract()
    {
        if (actionLock) return;
        if (interactionSensor.HasList == false) return;
        InputMove(Vector2.zero);
        interactionSensor.First.GetComponent<NavmeshMover>()?.TurnToFace((transform.position - interactionSensor.First.transform.position).ConvertToV2());

        interactionSensor.First.Interact();
    }

    public void InputCharge(float time)
    {
        if (time == 0f || actionLock || running) localCharge = 0f; else localCharge += time;

        if(NormalizedCharge > 0.4f && NormalizedCharge < 1f)
        {
            vfx.AttackCharge(true);
        }
        else if (NormalizedCharge == 1f && chargeReady == false)
        {
            chargeReady = true;
            vfx.ChargeReady();
            attackable.Flash(Color.white, 0.1f);
        }
        else if(NormalizedCharge <= 0.4f)
        {
            vfx.AttackCharge(false);
        }
    }

    public void InputFocus(float time)
    {
        //if (actionLock) time = 0;
        //ResetCombat();
        //anim.ResetTrigger("InputSpecial");
        //if (actionLock) return;

        if(time == 0)
        {
            ResetCombat();
        }

        //time *= (1f / focusTime);
        focusCharge += time;

        if (time == 0f)
        {
            vfx.SurgeCharge(false);
            anim.SetBool("InputFocus", false);
            focusCharge = time;
        }
        else if(NormalizedFocusCharge < 0.1f)
        {
            
            anim.SetBool("InputFocus", true);
            //if (vfx.SurgeCharge.isPlaying) vfx.SurgeCharge.Stop();
        }
        else if(NormalizedFocusCharge < 1f && LocalSP >= SURGECOST)
        {
            //anim.SetBool("StateCanSurge", true);
            //focusCharge += time * (1f / focusTime);
            vfx.SurgeCharge(true);
        }
        else if(NormalizedFocusCharge >= 1f && LocalSP >= SURGECOST)
        {
            //anim.SetBool("StateCanSurge", false);
            Surge();
        }
        else
        {
            vfx.SurgeCharge(false);
            //anim.SetBool("StateCanSurge", false);
        }

        anim.SetFloat("InputFocusTime", NormalizedFocusCharge);
    }

    void InputPain()
    {
        anim.SetTrigger("InputPain");

        ResetCombat();
    }

    void ResetCombat()
    {
        chargeReady = false;
        localCharge = 0f;
        focusCharge = 0f;
        anim.ResetTrigger("InputNormal");
        anim.ResetTrigger("InputNormalCharged");
        anim.ResetTrigger("InputSpecial");
        anim.ResetTrigger("InputSpecialCharged");
        anim.ResetTrigger("InputDodge");
        anim.SetBool("InputFocus", false);
        anim.SetBool("InputCastPrepare", false);
        StopRunning();
    }

    public void Hit(Object spawnObject)
    {
        //Debug.Log("Hit Call");
        hkaSel.Hit();

        LocalSP -= hkaSel.AL.cost;

        /*if (attackSelected == AttackClass.Normal)
        {
            LocalSP += ((HKAttackVolume)hkaSel).SPReward * GameMaster.I.Settings.MeleeSPGain;
        }*/
    }

    void OnMeleeHitMonster(Attackable monster)
    {
        //Debug.Log("Melee hit monster!");
        LocalSP += 1;
        HitStop(0.1f);
        if(monster.Heavy)
        {
            mover.PushDirection((transform.position - monster.transform.position).ConvertToV2(), 2.5f, 0.5f);
        }
    }

    public void Lunge(float distance)
    {
        if (hitStop || Pathfinding || Pushed) return;

        vfx.Step();
        vfx.Step();
        vfx.Step();
        mover.LungeDirection(transform.forward.ConvertToV2(), distance, 0.25f);
    }

    public void Image()
    {
        vfx.AfterImage.ShowImage();
    }

    public void SpawnEffect(Object spawnObject)
    {

    }

    public void Surge()
    {
        if (LocalSP >= SURGECOST)
        {
            attackable.Heal(attackable.SurgeHP);
            LocalSP -= SURGECOST;
            //focusCharge = 0f;
            focusCharge = 0.2f;
            //if (vfx.SurgeCharge != null) vfx.SurgeCharge.Stop();
            //anim.SetTrigger("FocusPop");
            vfx.SurgeHit();
            if (vfx.AfterImage != null) vfx.AfterImage.ShowImage(Color.green);
            attackable.Flash(Color.green);
            //HitStop(0.15f);
        }
        
    }

    void HitStop(float stopTime)
    {
        if (hitStop) return;
        StartCoroutine(DoHitStop(stopTime));
    }

    IEnumerator DoHitStop(float stoptime)
    {
        hitStop = true;
        float speed = anim.speed;
        anim.speed = 0;
        yield return null;
        yield return new WaitForSeconds(stoptime);
        anim.speed = speed;
        hitStop = false;
    }

    AttackData ProcessIncomingAttack(AttackData attack)
    {
        if(localArmor > 0)
        {
            int damage = attack.damageValue;
            attack.damageValue -= localArmor;
            LocalArmor -= damage;

            if (attack.damageValue < 0) attack.damageValue = 0;
        }
        return attack;
    }

    void OnIncomingAttack(AttackData attack)
    {
        InputPain();

        HitStop(0.2f * Mathf.Max(1, attack.damageValue));
        mover.FullStop();

        if (attackable.CurrentHP <= 0) anim.SetBool("StateDead", true);

        moveLock = true;
        //if (vfx.AfterImage != null) vfx.AfterImage.ShowImage(Color.white);

        mover.PushDirection((transform.position - attack.sourcePosition).ConvertToV2(), 2.5f * Mathf.Max(1, attack.damageValue), 0.5f);
        mover.OnPushComplete += OnKnockbackRecover;

        Attackable.IFrames(1.2f);
    }

    void OnKnockbackRecover()
    {
        mover.OnPushComplete -= OnKnockbackRecover;
        attackable.EvaluateLife();
    }

    public void PickupHealth()
    {
        Attackable.Heal(1);
    }

    public void PickupOverhealth()
    {
        LocalArmor += 1;
    }

    public void PickupEnergy()
    {
        LocalSP += 1;
    }

    public void Step()
    {
        vfx.Step();      
    }
}
