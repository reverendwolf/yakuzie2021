﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Attackable : MonoBehaviour
{
    [System.Serializable]
    private class Settings
    {
        public bool evaluateSelf; //will evaluate own life condition on taking damage (for simple destructables)
        public bool durable; //will not take damage unless the incoming damage exceeds the HP it currently has
        public bool meleeReward = true; //if true, attacking this with melee will reward you with SP. 
        public bool heavy = false; //if true, this will not be the subject of knockbacks ever

        public float iFrameTime = 0.25f;
        public GameObject deathEffect;

        public List<DamageType> weak = new List<DamageType>(); //double incoming damage from these types of damage
        public List<DamageType> immune = new List<DamageType>(); //zero damage from these types of damage
        public List<DamageType> resist = new List<DamageType>(); //take only 1 damage from these types of damage
    }


    const float FLASHTIME = 0.85f;
    public delegate void AttackEvent(AttackData attack);
    public delegate AttackData AttackProcessingEvent(AttackData attack);
    public delegate void IntEvent(int value);
    public delegate void Callback();

    public event AttackEvent OnAttack;
    public event AttackProcessingEvent IncomingAttack;
    public event IntEvent OnHeal;
    public event Callback OnAttackEvent;
    public event Callback OnDeathEvent;

    [SerializeField] Settings settings;
    [SerializeField] int maxHP = 1;
    [SerializeField] int localHP;
    [SerializeField] IntegerMonitor hpMonitor;
    //[SerializeField] DamageType counterAttack;
    EPOOutline.Outlinable outlinable;
    [SerializeField] AudioClip onHitSound;
    AudioSource audioSource;
    

    public int MaxHP => maxHP;
    public int CurrentHP => localHP;
    public float CurrentHPPercent => (float)localHP / (float)maxHP;
    public float CurrentDamagePercent => 1 - CurrentHPPercent;
    public int SurgeHP => Mathf.FloorToInt((float)maxHP * 0.25f);
    public Vector3 ApparentPosition => transform.position; //will modify this later - dodges will change the apparent position
    public bool SPReward => settings.meleeReward;
    public bool Heavy => settings.heavy;

    float iframes;
    public bool HasIFrames => iframes > 0f;
    float osc;
    float freq = 60;
    float deathTime;
    public float DeathTime => deathTime;
    bool alive;
    public bool IsAlive => alive;
    //public DamageType CounterAttack => counterAttack;
    bool doFlash = false;

    private void OnEnable()
    {
        localHP = maxHP;
        alive = true;
    }

    private void Start()
    {
        if (hpMonitor != null) hpMonitor.SetValue(localHP);
        outlinable = GetComponentInChildren<EPOOutline.Outlinable>();
        audioSource = GetComponent<AudioSource>();
    }

    private void Update()
    {
        if (iframes >= 0f)
        {
            if(doFlash)
            {
                osc += Time.deltaTime;
                if (Mathf.Sin(osc * freq) > 0)
                {
                    outlinable?.FrontParameters.FillPass.SetColor("_PublicColor", new Color(1, 1, 1, 0.4f));
                }
                else
                {
                    outlinable?.FrontParameters.FillPass.SetColor("_PublicColor", Color.clear);
                }
            }
            
            iframes -= Time.deltaTime;

            if(iframes < 0f && doFlash)
            {
                outlinable?.FrontParameters.FillPass.SetColor("_PublicColor", Color.clear);
                doFlash = false;
            }

            if (iframes <= 0f && CurrentHP <= 0) EvaluateLife();
        }
        

        //if (iframes >= 0f) outlinable.FrontParameters.FillPass.SetColor("_PublicColor", Color.green);
        //else outlinable.FrontParameters.FillPass.SetColor("_PublicColor", new Color(0,0,0,0));
    }

    public void Damage(AttackData attack)
    {
        if (CurrentHP <= 0) return;
        if (iframes > 0f) return;
        if (IncomingAttack != null) attack = IncomingAttack(attack);
        int value = attack.damageValue;

        if(settings.resist.Contains(attack.damageType))
        {
            value = 1;
            attack.knockback = false;
        }

        if(settings.immune.Contains(attack.damageType))
        {
            value = 0;
            attack.knockback = false;
        }

        if(settings.weak.Contains(attack.damageType))
        {
            value *= 2;
            attack.knockback = true;
        }

        //UINumberPopper.I.PopNumber(value, this.transform, attack.critical ? Color.yellow : Color.white);
        if(settings.durable == false || settings.durable && value >= CurrentHP) UpdateHP(Mathf.Max(localHP - value, 0));
        OnAttack?.Invoke(attack);
        OnAttackEvent?.Invoke();
        audioSource?.PlayOneShot(onHitSound);
        //Flash(Color.white, iFrameTime);

        if (settings.evaluateSelf) EvaluateLife();
        
        IFrames(settings.iFrameTime);
        if(gameObject.CompareTag("Player"))
        {
            doFlash = true;
        }
        else
        {
            Flash(Color.red, settings.iFrameTime);
        }
         
    }

    public void Flash(Color color, float time = FLASHTIME)
    {
        if (outlinable == null) return;
        if (this.isActiveAndEnabled == false) return;
        if (flash != null) StopCoroutine(flash);
        flash = StartCoroutine(DoFlash(color, time));
    }

    Coroutine flash;

    IEnumerator DoFlash(Color color, float time)
    {
        if (outlinable)
        {
            Color c = color;
            float t = time;
            outlinable.FrontParameters.FillPass.SetColor("_PublicColor", c);
            outlinable.OutlineParameters.FillPass.SetColor("_PublicColor", c);
            while (t > 0f)
            {
                c = new Color(c.r, c.g, c.b, Mathf.MoveTowards(c.a, 0f, 1 - (t / time)));
                outlinable.FrontParameters.FillPass.SetColor("_PublicColor", c);
                outlinable.OutlineParameters.FillPass.SetColor("_PublicColor", c);
                t -= Time.deltaTime;
                yield return null;
            }
        }
        flash = null;
    }

    public void Heal(int heal)
    {
        OnHeal?.Invoke(heal);
        UpdateHP(Mathf.Min(localHP + heal, maxHP));
    }

    void UpdateHP(int newHP)
    {
        localHP = Mathf.Clamp(newHP, 0, maxHP);
        if (hpMonitor != null) hpMonitor.SetValue(localHP);
    }

    public void UpdateMaxHP(int newMaxHP)
    {
        Debug.Log("New Max HP:" + newMaxHP);
        maxHP = newMaxHP;
        UpdateHP(localHP);
    }

    public void IFrames(float iTime)
    {
        iframes = iTime;
        osc = 0;
    }

    public void EvaluateLife()
    {
        if (localHP <= 0)
        {
            deathTime = Time.timeSinceLevelLoad;
            
            if (settings.deathEffect != null) Instantiate(settings.deathEffect, transform.position + settings.deathEffect.transform.position, settings.deathEffect.transform.rotation);
            int drop = Random.Range(1, 21);
            /*
            if (drops.Length > 0 && drop <= dropChance)
            {
                if(dropAll)
                {
                    for (int i = 0; i < drops.Length; i++)
                    {
                        GameMaster.I.SpawnPickup(drops[i], transform.position + Vector3.up + Random.insideUnitSphere);
                    }
                }
                else
                {
                    GameMaster.I.SpawnPickup(drops[Random.Range(0, drops.Length)], transform.position + Vector3.up);
                }
                
            }
            */

            OnDeathEvent?.Invoke();
            alive = false;
            gameObject.SetActive(false);
        }
    }

    private void OnDrawGizmos()
    {
        if (Application.isPlaying && gameObject.GetComponent<Renderer>() != null && gameObject.GetComponent<Renderer>().isVisible)
            Gizmos.color = Color.green;
        else
            Gizmos.color = Color.blue;
        Gizmos.DrawCube(transform.position, Vector3.one * 0.1f);
    }
}