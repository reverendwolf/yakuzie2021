﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(MeshFilter))]
[RequireComponent(typeof(MeshRenderer))]
public class HKAfterImage : MonoBehaviour
{
    [SerializeField] SkinnedMeshRenderer link;
    MeshFilter filter;
    MeshRenderer rend;
    Color defaultColor;
    Transform parent;
    // Start is called before the first frame update
    void Start()
    {
        parent = transform.parent;
        filter = GetComponent<MeshFilter>();
        rend = GetComponent<MeshRenderer>();
        defaultColor = rend.material.color;
        rend.enabled = false;
    }

    public void ShowImage(Color color)
    {
        LeanTween.cancel(this.gameObject, true);

        link.BakeMesh(filter.mesh);

        transform.localScale = Vector3.one * 1.1f;
        transform.localPosition = Vector3.zero;
        transform.localRotation = link.transform.localRotation;
        transform.parent = null;

        rend.material.color = color;
        rend.enabled = true;

        LeanTween.scale(this.gameObject, Vector3.one * 1.15f, 0.75f).setEaseOutCubic();
        LeanTween.color(this.gameObject, new Color(color.r, color.g, color.b, 0f), 0.7f).setEaseInOutQuad().setOnComplete(Recouple);
    }

    public void ShowImage()
    {
        ShowImage(defaultColor);
    }

    void Recouple()
    {
        transform.parent = parent;
        rend.enabled = false;
    }
}
