﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public struct AttackData
{
    public int damageValue;
    public DamageType damageType;
    public bool knockback;
    public Vector3 sourcePosition;
    public Transform ownerTransform; 

    public AttackData(int dmgval, DamageType dtype, bool kb, Vector3 source, Transform owner)
    {
        damageType = dtype;
        damageValue = dmgval;
        knockback = kb;
        sourcePosition = source;
        ownerTransform = owner;
    }
}
