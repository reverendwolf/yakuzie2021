﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IAnimEvents
{
    void MovementLock(bool input);

    void ActionLock(bool input);

    void Hit(Object messageObject);

    void Lunge(float strength);

    void Image();

    void Surge();

    void Step();
}
