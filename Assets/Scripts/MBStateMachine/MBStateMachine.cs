﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MBStateMachine : MonoBehaviour
{

    public delegate void OnStateChanged(BaseState newState);
    public OnStateChanged onStateChanged;

    public virtual BaseState CurrentState
    {
        get { return currentState; }
        set { Transition(value); }
    }

    BaseState currentState;
    bool inTransition;

    public virtual T GetState<T>() where T : BaseState
    {
        T target = GetComponent<T>();
        if (target == null)
        {
            target = gameObject.AddComponent<T>();
        }
        return target;
    }

    public virtual void ChangeState<T>() where T : BaseState
    {
        CurrentState = GetState<T>();
    }

    protected virtual void Transition(BaseState newState)
    {
        if (currentState == newState || inTransition)
            return;

        inTransition = true;

        if (currentState != null)
            currentState.Exit();

        currentState = newState;

        if (currentState != null)
        {
            currentState.Enter();
            if (onStateChanged != null)
                onStateChanged(CurrentState);
        }

        inTransition = false;
    }

    void Update()
    {
        if (currentState != null && inTransition == false)
        {
            currentState.Tick();
        }
    }
}