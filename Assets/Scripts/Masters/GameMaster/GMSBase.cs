﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GMSBase : BaseState
{
    protected GameMaster GM => GetComponent<GameMaster>();
    protected UIMaster UI => UIMaster.I;
}
