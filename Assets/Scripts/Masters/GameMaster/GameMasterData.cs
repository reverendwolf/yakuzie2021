﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameMasterData : MonoBehaviour
{
    [SerializeField]
    GameData gamedata;

    public GameData GameData => gamedata;

    public void OnValidate()
    {
        gamedata.ManualOnDataUpdate();
    }
}
