﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using Cinemachine;

public class GameMaster : MBStateMachine
{
    private enum PlayableCharacters
    {
        Motoko,
    }

    public delegate void GMEvent();
    public delegate void GMToggleEvent(bool toggle);

    public static event GMToggleEvent OnPauseGame;
    public static event GMEvent OnGMReady;

    private static GameMaster _instance;
    public static GameMaster I
    {
        get
        {
            if (!_instance)
            {
                _instance = GameObject.FindObjectOfType(typeof(GameMaster)) as GameMaster;
            }

            if (!_instance)
            {
                //Debug.LogError("There is no GameMaster object!");
            }

            return _instance;
        }
    }

    PlayableCharacters playerID;
    bool pauseState = false;
    bool pauseMonsters = false;
    public bool PauseState => pauseState || pauseMonsters;
    GameMasterData gameMasterData;
    public GameData SaveData => gameMasterData.GameData;
    public GameMasterData DataMaster => GetComponent<GameMasterData>();
    [SerializeField] HKPlayerData[] playerData;
    public GameObject PlayerGOSource => PlayerData.PlayerObject;
    public HKPlayerData PlayerData => playerData[(int)playerID];
    public Transform PlayerTransform => playerObject;
    public HKPlayer HKPlayer => playerObject.GetComponent<HKPlayer>();
    [SerializeField] Transform playerObject;
    public CinemachineOrbitalTransposer DungeonCam => dungeonCam.GetCinemachineComponent<CinemachineOrbitalTransposer>();
    public CinemachineVirtualCamera DungeonCamDirect => dungeonCam;
    [SerializeField] CinemachineVirtualCamera dungeonCam;
    public CinemachineVirtualCamera DialogueCam => dialogueCam;
    [SerializeField] CinemachineVirtualCamera dialogueCam;
    [SerializeField] Transform cameraTarget;
    [SerializeField] Transform spellTarget;
    public Transform SpellTarget => spellTarget;

    public MapMaster CurrentMap => currentMap;
    MapMaster currentMap;

    [SerializeField] GameMasterSettings settings;
    public GameMasterSettings Settings => settings;
    [SerializeField] string[] gameFlags;
    public string[] GameFlags => gameFlags;

    [SerializeField] GameObject pickupObject;
    public GameObject PickupObject => pickupObject;
    List<Pickup> pickupPool = new List<Pickup>();

    public bool StateLocked => stateLocked;
    bool stateLocked = false;

    int wallet;
    [SerializeField] IntegerMonitor walletMonitor;
    public int Wallet
    {
        get { return wallet; }
        set { wallet = value; if (walletMonitor != null) walletMonitor.SetValue(wallet); }
    }
    int experience;
    [SerializeField] IntegerMonitor expMonitor;
    int Experience
    {
        get { return experience; }
        set { experience = value; if (expMonitor != null) expMonitor.SetValue(experience); }
    }
    public int CurrentExperience => experience;

    public void OnEnable()
    {
        if (I != null && I != this)
            DestroyImmediate(this.gameObject);
    }

    private void OnDestroy()
    {
        //PauseGame = null;
        //ResumeGame = null;
        gameMasterData.GameData.OnDataUpdate -= GameDataUpdated;
    }

    private void Awake()
    {
        gameMasterData = GetComponent<GameMasterData>();
        
    }

    public void Start()
    {
        //GameMaster.OnPauseGame += BoolTest;
        Application.targetFrameRate = 70;
        Wallet = 0;
        //Debug.Log("GameMaster Scene: " + this.gameObject.scene.name);
        //Debug.Log("Active Scene: " + SceneManager.GetActiveScene().name);
        //InitializeGameFlags();
        OnGMReady?.Invoke();
        gameMasterData.GameData.OnDataUpdate += GameDataUpdated;
        ChangeState<GMSTitleScreen>();
    }

    public void GameDataUpdated()
    {
        //Debug.Log("Game Master: Game Data Updated");
        playerObject.GetComponent<Attackable>().UpdateMaxHP(gameMasterData.GameData.MaxHealth);
    }

    public void RegisterPlayerObject(GameObject playerObject)
    {
        this.playerObject = playerObject.transform;
        cameraTarget.parent = this.playerObject.transform;
        cameraTarget.localPosition = Vector3.zero;
    }

    void BoolTest(bool test)
    {
        //sDebug.Log("Bool test: " + test);
    }

    public void RegisterMapMaster(MapMaster newMap)
    {
        currentMap = newMap;
    }

    public void LockState()
    {
        SetStateLock(true);
    }

    public void UnlockState()
    {
        SetStateLock(false);
    }

    void SetStateLock(bool locked)
    {
        stateLocked = locked;
    }

    public void LaunchConversation(MOStory convo)
    {
        LaunchConversation(convo.Chart, convo.Slug);
        //if (convo.InteractionType == BaseMapObject.Interaction.AutoSingle || convo.InteractionType == BaseMapObject.Interaction.ManualOnce) convo.gameObject.SetActive(false);
    }

    public void LaunchConversation(Fungus.Flowchart chart, string slug)
    {
        if (CurrentState.GetType() == typeof(GMSWalk) || CurrentState.GetType() == typeof(GMSMissionStart))
        {
            //Debug.Log("Launch Convo");
            ChangeState<GMSInteract>();
            GMSInteract talkState = GetComponent<GMSInteract>();
            talkState.LaunchFlowchart(chart, slug);
            //dialogueCam.gameObject.SetActive(true);
        }
        else
        {
            Debug.Log("Launch Convo Ignored");
        }
    }

    public void EndInteraction()
    {
        if (CurrentState.GetType() == typeof(GMSInteract))
        {
            //Debug.Log("End Interaction");
            ChangeState<GMSWalk>();
        }
        else
        {
            Debug.Log("End Convo Ignored");
        }
    }

    public void InputPause(bool pause)
    {
        OnPauseGame?.Invoke(pause);
        pauseState = pause;
        //Time.timeScale = pause ? 0 : 1;
    }

    public void DoPauseMonsters(bool pause)
    {
        OnPauseGame?.Invoke(pause);
        pauseMonsters = pause;
    }

    public void LaunchModule(GameModuleBase module)
    {
        if (CurrentState.GetType() == typeof(GMSInteract))
        {
            Debug.Log("Interrupt Interaction: " + module.name);
            ((GMSInteract)CurrentState).LaunchModule(module);
        }
        else if (CurrentState.GetType() == typeof(GMSWalk))
        {
            Debug.Log("Launch Module: " + module.name);
            ChangeState<GMSInteract>();
            ((GMSInteract)CurrentState).LaunchModule(module);
        }
        else
        {
            Debug.Log("Launch Module Ignored");
        }
    }

    public void RegionTransition(MapRegion targetRegion, Vector3 targetPosition)
    {
        if (CurrentState.GetType() == typeof(GMSWalk))
        {
            //Debug.Log("Region Transition: " + CurrentMap.CurrentRegion.RegionName + " -> " + regionGate.TargetRegion.RegionName);
            for (int i = 0; i < pickupPool.Count; i++)
            {
                pickupPool[i].gameObject.SetActive(false);
            }
            ChangeState<GMSRegionTrans>();
            GMSRegionTrans transitionState = GetComponent<GMSRegionTrans>();
            transitionState.RegionTransition(targetRegion, targetPosition);
        }
        else
        {
            Debug.Log("Region Transition Ignored");
        }
    }

    public void EndModule()
    {
        ChangeState<GMSWalk>();
    }

    public void SpawnPickup(Pickup.PickupType type, Vector3 position)
    {
        Pickup nextPickup = null;
        for (int i = 0; i < pickupPool.Count; i++)
        {
            if(pickupPool[i].gameObject.activeInHierarchy == false)
            {
                nextPickup = pickupPool[i];
                nextPickup.gameObject.SetActive(true);
                break;
            }
        }

        if(nextPickup == null)
        {
            pickupPool.Add(Instantiate(pickupObject, position, Quaternion.identity, this.transform).GetComponent<Pickup>());
            nextPickup = pickupPool[pickupPool.Count - 1];
        }
        
        nextPickup.SetPickup(type);
        nextPickup.transform.position = position;
        nextPickup.Pop();
        
    }

    public void GrantExperience(int expValue)
    {
        Experience += expValue;
    }

    public void SpendExperience(int expCost)
    {
        Experience -= expCost;
    }

    public void HandlePickup(Pickup.PickupType type)
    {
        if (CurrentState.GetType() == typeof(GMSWalk))
        {
            switch (type)
            {
                case Pickup.PickupType.Health:
                    HKPlayer.PickupHealth();
                    break;
                case Pickup.PickupType.Copper:
                    Wallet += 1;
                    break;
                case Pickup.PickupType.Silver:
                    Wallet += 5;
                    break;
                case Pickup.PickupType.Gold:
                    Wallet += 25;
                    break;
                case Pickup.PickupType.Platinum:
                    Wallet += 100;
                    break;
                case Pickup.PickupType.Energy:
                    HKPlayer.PickupEnergy();
                    break;
                case Pickup.PickupType.Overhealth:
                    HKPlayer.PickupOverhealth();
                    break;
            }
        }
    }

    public void WarpPlayer(Vector3 position)
    {
        Vector3 warpDelta = position - PlayerTransform.position;

        HKPlayer.Mover.TeleportTo(position);
        DungeonCamDirect.OnTargetObjectWarped(PlayerTransform, warpDelta);
    }

    public void LoadSession(int sessionToLoad)
    {
        if (CurrentState.GetType() == typeof(GMSLoadMission)) return;

        int stl = sessionToLoad + 2;
        //sessions will be numbered in order starting from 2 because the master scene is 0 and the title screen is 1
        if(stl < SceneManager.sceneCountInBuildSettings)
        {
            Debug.Log("Request to load Scene "  + stl);
            if (CurrentState.GetType() == typeof(GMSInteract)) GetComponent<GMSInteract>().StopAllCoroutines();
            Debug.Log("State check passed");
            cameraTarget.parent = this.transform;
            Debug.Log("Changing State");
            ChangeState<GMSLoadMission>();
            GetComponent<GMSLoadMission>().LoadMission(stl);
        }
        else
        {
            Debug.Log("Request to load session too high");
        }
        
    }

    /*IEnumerator MapTransition(Helper.MapRegistry newMap, Helper.Cardinal entry)
    {
        Debug.Log("Current Map: " + currentMap.MapName);
        Debug.Log("Starting Unload");
        
        yield return SceneManager.UnloadSceneAsync(SceneManager.GetActiveScene());
        Debug.Log("Unload Complete");
        Debug.Log("Starting Load");
        yield return SceneManager.LoadSceneAsync(newMap.ToString(), LoadSceneMode.Additive);
        Debug.Log("Load Complete");
        SceneManager.SetActiveScene(SceneManager.GetSceneByName(newMap.ToString()));
        Debug.Log("Active Scene Set");
        yield return null;
        Debug.Log("Current Map: " + currentMap.MapName);
        Debug.Log("Set Player to Entrance Point.");
        //playerObject.position = currentMap.GetEntryPoint(entry);
        //playerObject.rotation = currentMap.GetEntryFacing(entry);
    }*/

    public void CameraLookAt(Vector3 position, float waitTime = 1f)
    {
        LeanTween.move(cameraTarget.gameObject, position, waitTime).setEaseInOutQuad();
        //cameraTarget.position = position;
    }

    public void CameraReset(float waitTime = 1f)
    {
        LeanTween.moveLocal(cameraTarget.gameObject, Vector3.zero, waitTime).setEaseInOutQuad();
        //cameraTarget.localPosition = Vector3.zero;
    }
}
