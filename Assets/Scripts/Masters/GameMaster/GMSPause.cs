﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GMSPause : GMSBase
{
    public override void Enter()
    {
        base.Enter();
        UI.ShowPauseMenu(true);
        GM.InputPause(true);
    }

    public override void Exit()
    {
        base.Exit();
        UI.ShowPauseMenu(false);
        GM.InputPause(false);
    }

    public override void AddListeners()
    {
        base.AddListeners();
        InputMaster.OnActionPressed += ButtonInput;
    }

    public override void RemoveListeners()
    {
        base.RemoveListeners();
        InputMaster.OnActionPressed -= ButtonInput;
    }

    void ButtonInput(InputMaster.ButtonInput button, InputMaster.ButtonModifier modifier)
    {
        switch(button)
        {
            case InputMaster.ButtonInput.Start:
                GM.ChangeState<GMSWalk>();
                break;
        }
    }


}
