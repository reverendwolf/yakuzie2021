﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GMSTitleScreen : GMSBase
{
    bool screenReady;

    public override void Enter()
    {
        base.Enter();
        StartCoroutine(StartTitleScreen());
        UIMaster.I.SnapToBlack();
        UIMaster.I.ShowSessionScreen(false);
    }

    public override void Exit()
    {
        base.Exit();
    }

    public override void AddListeners()
    {
        base.AddListeners();
        InputMaster.OnActionPressed += ButtonInput;
    }

    public override void RemoveListeners()
    {
        base.RemoveListeners();
        InputMaster.OnActionPressed -= ButtonInput;
    }

    IEnumerator StartTitleScreen()
    {
        if(SceneManager.sceneCount > 1)
        {
            Debug.Log("Too many scenes");
            //find which scene is the gamemaster
            //unload the other one
            for (int i = 0; i < SceneManager.sceneCount; i++)
            {
                Scene s = SceneManager.GetSceneAt(i);
                Debug.Log("scene: " + s.name);
                if (s != SceneManager.GetSceneByBuildIndex(0))
                {
                    Debug.Log("Unloading " + s.name);
                    yield return SceneManager.UnloadSceneAsync(s);
                }
            }
        }

        Debug.Log("Title Screen Time");
        //load the title screen scene
        //0 will always be the game master, 1 will always be the title screen
        yield return SceneManager.LoadSceneAsync(1, LoadSceneMode.Additive);

        SceneManager.SetActiveScene(SceneManager.GetSceneAt(1));
        Debug.Log("Active Scene set: " + SceneManager.GetActiveScene().name);


        UIMaster.I.FadeToView();

        yield return null;
        yield return null;

        UIMaster.I.ShowTitleScreen(true);

        yield return UIMaster.I.FadeToView();

        screenReady = true;
    }

    void ButtonInput(InputMaster.ButtonInput button, InputMaster.ButtonModifier modifier)
    {
        if (!screenReady) return;
        switch(UIMaster.I.Title.State)
        {
            case GUITitleMain.ScreenState.MAIN:
                if (button == InputMaster.ButtonInput.Up) UIMaster.I.Title.SelectUp();

                if (button == InputMaster.ButtonInput.Down) UIMaster.I.Title.SelectDown();
                
                break;
            case GUITitleMain.ScreenState.OPTIONS:
                
                break;
            case GUITitleMain.ScreenState.STORY:
                if (button == InputMaster.ButtonInput.Right) UIMaster.I.Title.SelectDown();

                if (button == InputMaster.ButtonInput.Left) UIMaster.I.Title.SelectUp();

                if(button == InputMaster.ButtonInput.Cross)
                {
                    Debug.Log("Select Session " + UIMaster.I.Title.SessionSelection);
                    StartCoroutine(DoLoadSession());
                }

                break;
            case GUITitleMain.ScreenState.CREDITS:
                break;
        }

        if (button == InputMaster.ButtonInput.Cross) UIMaster.I.Title.SelectCurrentOption();

        if(button == InputMaster.ButtonInput.Circle && UIMaster.I.Title.State != GUITitleMain.ScreenState.MAIN)
        {
            UIMaster.I.Title.SelectScreenState(GUITitleMain.ScreenState.MAIN);
        }
    }

    IEnumerator DoLoadSession()
    {
        screenReady = false;
        //UIMaster.I.Title.End();
        yield return UIMaster.I.FadeToBlack();
        UIMaster.I.ShowTitleScreen(false);
        GM.LoadSession(UIMaster.I.Title.SessionSelection);
    }
}
