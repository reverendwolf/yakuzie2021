﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GMSMissionStart : GMSBase
{
    public delegate void Callback();
    public static event Callback OnMissionStart;
    public static event Callback OnMissionInit;

    public override void Enter()
    {
        base.Enter();
        UIMaster.I.SnapToBlack();
        StartCoroutine(StartMission());
    }



    IEnumerator StartMission()
    {
        OnMissionInit?.Invoke();
        //GameMaster.I.WarpPlayer(MapMaster.I.StartPosition);
        yield return null;
        OnMissionStart?.Invoke();
        
        yield return null;
        if (MapMaster.I.Quest != null && MapMaster.I.MissionChart.HasBlock("Mission Start"))
        {
            GM.LaunchConversation(MapMaster.I.MissionChart, "Mission Start");
        }
        else
        {
            GM.ChangeState<GMSWalk>();
        }

    }


}
