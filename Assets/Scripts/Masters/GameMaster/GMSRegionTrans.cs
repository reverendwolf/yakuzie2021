﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GMSRegionTrans : GMSBase
{
    public void RegionTransition(MapRegion targetRegion, Vector3 targetPosition)
    {
        StartCoroutine(DoTransition(targetRegion, targetPosition));
    }

    IEnumerator DoTransition(MapRegion targetRegion, Vector3 targetPosition)
    {
        GM.HKPlayer.Mover.ForceStop();
        yield return null;
        yield return StartCoroutine(UIMaster.I.FadeToBlack());
        Vector3 warpDelta = targetPosition - GM.PlayerTransform.position;

        GM.CurrentMap.RegionTransision(targetRegion);
        GM.WarpPlayer(targetPosition);

        

        yield return new WaitForSeconds(0.5f);
        yield return StartCoroutine(UIMaster.I.FadeToView());
        UIMaster.I.ShowTopMessage(targetRegion.RegionName);
        GM.ChangeState<GMSWalk>();
    }
}
