﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Fungus;

public class GMSInteract : GMSBase
{
    Flowchart runningChart;
    GameModuleBase runningModule;
    bool canMoveCamera;
    public void LaunchFlowchart(Flowchart chart, string slug)
    {
        GM.DialogueCam.gameObject.SetActive(true);
        runningChart = chart;
        Debug.Log("Launch Flowchart: " + chart.name + ", " + slug);
        if (slug == "")
        {
            Debug.LogWarning("Slug name is empty!");
        }
        chart.ExecuteIfHasBlock(slug);
        StartCoroutine(RunFlowchart(chart, slug));
        //canMoveCamera = true;
    }

    public void LaunchModule(GameModuleBase module)
    {
        Debug.Log("Launch Module: " + module.name);
        runningModule = module;
        module.OnModuleEnd += CompleteModule;
        module.LaunchModule(GM.HKPlayer);
    }

    public void CompleteModule(int endSignal)
    {
        runningModule.OnModuleEnd -= CompleteModule;
        Debug.Log("Module ended with signal: " + endSignal);
        if(runningChart == null)
        {
            GM.EndInteraction();
        }
    }

    public override void Enter()
    {
        base.Enter();
        canMoveCamera = true;
        //Debug.Log("GMSTalk");
        GM.LockState();
        UI.SuspendVerb(true);
        GM.DoPauseMonsters(true);
    }

    public override void Exit()
    {
        base.Exit();
        runningChart = null;
        runningModule = null;
        UI.SuspendVerb(false);
        GM.DialogueCam.gameObject.SetActive(false);
    }

    public override void AddListeners()
    {
        base.AddListeners();
        InputMaster.RightStickDirection += RightStickDirection;
    }

    public override void RemoveListeners()
    {
        base.RemoveListeners();
        InputMaster.RightStickDirection -= RightStickDirection;
    }

    IEnumerator RunFlowchart(Flowchart chart, string slug)
    {
        yield return null;
        while (chart != null && chart.HasExecutingBlocks())
        {
            yield return null;
        }
        //GM.UnlockState();
        GM.EndInteraction();
    }

    void RightStickDirection(Vector2 direction)
    {
        if(canMoveCamera) GM.DungeonCam.m_XAxis.Value = direction.x * GM.Settings.CameraSpeed * 0.5f;
    }
}
