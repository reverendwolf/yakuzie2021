﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Cinemachine;

public class GMSWalk : GMSBase
{
    const float MELEECHARGE = 1.5f;
    const float SPECIALCHARGE = 1.2f;
    const float HEALCHARGE = 1f;
    //private ACTPlayer PA => GM.PlayerActor;
    private HKPlayer HK => GM.HKPlayer;
    private CinemachineOrbitalTransposer playerCamera;
    bool[] Skills => GM.SaveData.Skills;

    Vector2 lastInput;
    int lastMissionPhase = -10;
    bool holdNormal;
    bool holdFocus;
    public override void Enter()
    {
        base.Enter();

        playerCamera = GM.DungeonCam;
        //Debug.Log("GMS-Walk");
        Invoke("ShowMissionObjective", 1f);
        GM.DoPauseMonsters(false);
    }

    public override void Exit()
    {
        base.Exit();
        HK.InputMove(Vector2.zero);
        HK.InputCharge(0f);
        HK.InputFocus(0f);
        HK.Mover.ForceStop();
        holdNormal = false;
        holdFocus = false;
        CancelInvoke();
        //GM.DoPauseMonsters(true);
    }

    public override void AddListeners()
    {
        base.AddListeners();
        InputMaster.LeftStickRelativeDirection += LeftStickDirection;
        InputMaster.RightStickDirection += RightStickDirection;
        InputMaster.OnActionPressed += ButtonInput;
        InputMaster.OnActionReleased += ButtonRelease;
    }

    public override void RemoveListeners()
    {
        base.RemoveListeners();
        InputMaster.LeftStickRelativeDirection -= LeftStickDirection;
        InputMaster.RightStickDirection -= RightStickDirection;
        InputMaster.OnActionPressed -= ButtonInput;
        InputMaster.OnActionReleased -= ButtonRelease;
    }

    private void Update()
    {
        //Debug.Log("Dialogue Cam: " + GM.DialogueCam.gameObject.activeInHierarchy);

        if (holdNormal)
        {
            HK.InputCharge(Time.deltaTime / MELEECHARGE);
        }
        else if(holdFocus)
        {
            HK.InputFocus(Time.deltaTime / HEALCHARGE);
        }
    }

    void ShowMissionObjective()
    {
        
    }

    void LeftStickDirection(Vector2 direction)
    {
        HK.InputMove(direction);
        lastInput = direction;
        //Debug.DrawRay(PA.transform.position + Vector3.up, new Vector3(lastInput.x, PA.transform.position.y, lastInput.y), Color.white, Time.deltaTime);
    }

    void RightStickDirection(Vector2 direction)
    {
        //if(playerCamera.VcamState.)
        playerCamera.m_XAxis.Value = direction.x * GM.Settings.CameraSpeed;
    }

    void ButtonInput(InputMaster.ButtonInput button, InputMaster.ButtonModifier modifier)
    {
        //Debug.Log("Btn: " + button.ToString() + " Mod: " + modifier.ToString());

        if( modifier == InputMaster.ButtonModifier.None)
        {
            switch (button)
            {
                case InputMaster.ButtonInput.Cross:
                    if (HK.ActionLocked == false)
                    {
                        HK.InputInteract();
                    }
                    break;
                case InputMaster.ButtonInput.Circle:
                    if (HK.ActionLocked == false)
                    {
                        if (lastInput != Vector2.zero)
                        {
                            HK.InputDodge(lastInput);
                        }
                        else
                        {
                            HK.InputDodge(HK.transform.forward.ConvertToV2());
                        }
                        HK.InputCharge(0);
                    }
                    break;
                case InputMaster.ButtonInput.Square:
                    if (HK.ActionLocked == false)
                    {
                        HK.InputCharge(0);
                        HK.InputNormal();
                        holdNormal = true;
                    }
                    break;
                case InputMaster.ButtonInput.Triangle:
                    if (HK.ActionLocked == false)
                    {
                        HK.InputCharge(0);
                        HK.InputHeavy();
                    }
                    break;
                case InputMaster.ButtonInput.R2:
                    if (HK.ActionLocked == false)
                    {
                        HK.InputCharge(0);
                        holdFocus = true;
                    }
                    break;
                case InputMaster.ButtonInput.L2:
                    break;
                case InputMaster.ButtonInput.Start:
                    //HK.InputCharge(0);
                    GM.ChangeState<GMSPause>();
                    break;
                case InputMaster.ButtonInput.L1:
                    //HK.InputCharge(0);
                    if(HK.ActionLocked == false && HK.MoveLocked == false && Skills[(int)Helper.UnlockList.Cast + 2] && HK.CanCast)
                    {
                        GM.ChangeState<GMSTarget>();
                    }
                    break;
                case InputMaster.ButtonInput.R1:
                    if (HK.ActionLocked == false && HK.MoveLocked == false && Skills[(int)Helper.UnlockList.Special + 2] && HK.CanSpecial)
                    {
                        GM.ChangeState<GMSTarget>();
                    }
                    break;
            }
        }
        
    }

    void ButtonRelease(InputMaster.ButtonInput button, InputMaster.ButtonModifier modifier)
    {
        switch (button)
        {
            case InputMaster.ButtonInput.Square:
                holdNormal = false;
                HK.InputNormal();
                HK.InputCharge(0f);
                break;
            case InputMaster.ButtonInput.R2:
                holdFocus = false;
                HK.InputFocus(0f);
                break;
            case InputMaster.ButtonInput.L2:
                break;
        }

    }


}
