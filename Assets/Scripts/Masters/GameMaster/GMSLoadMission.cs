﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GMSLoadMission : GMSBase
{

    /*public void LoadMission(string sceneName)
    {
        Debug.Log("Loading " + sceneName);
        StartCoroutine(DoLoadMission(sceneName));
    }*/

    public void LoadMission(int sceneIndex)
    {
        Debug.Log("Load Mission Called");
        StartCoroutine(DoLoadMission(sceneIndex));
    }

    IEnumerator DoLoadMission(int sceneIndex)
    {
        Debug.Log("Fade Out Start");
        yield return UIMaster.I.FadeToBlack();
        yield return StartCoroutine(SceneTransition(sceneIndex));
        Debug.Log("Mission Start!");
        GM.ChangeState<GMSMissionStart>();
    }

    IEnumerator SceneTransition(int sceneIndex)
    {
        Debug.Log("GM Scene BI: " + this.gameObject.scene.buildIndex);
        Debug.Log("Active Scene BI: " + SceneManager.GetActiveScene().buildIndex);

        if (this.gameObject.scene != SceneManager.GetActiveScene())
        {
            Debug.Log("Unloading active scene " + SceneManager.GetActiveScene().name);
            yield return SceneManager.UnloadSceneAsync(SceneManager.GetActiveScene());
        }

        Debug.Log("Loading scene " + sceneIndex);
        yield return SceneManager.LoadSceneAsync(sceneIndex, LoadSceneMode.Additive);
        Debug.Log("Setting Active Scene");
        SceneManager.SetActiveScene(SceneManager.GetSceneByBuildIndex(sceneIndex));
        yield return null;
        yield return null;
        yield return null;
        UIMaster.I.ShowSessionScreen(true);
    }
}
