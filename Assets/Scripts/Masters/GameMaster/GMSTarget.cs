﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GMSTarget : GMSBase
{
    HKPlayer Player => GM.HKPlayer;
    Transform Target => GM.SpellTarget;
    bool[] Skills => GM.SaveData.Skills;

    public override void Enter()
    {
        base.Enter();
        //GM.HKPlayer.InputMove(Vector2.zero);
        Target.gameObject.SetActive(true);
        Target.position = Player.transform.position;
        Time.timeScale = 0.1f;
        Player.InputCastPrepare(true);
        //GM.DoPauseMonsters(true);
    }

    public override void Exit()
    {
        base.Exit();
        Time.timeScale = 1f;
        //Player.InputCastPrepare(false);
        Target.gameObject.SetActive(false);
        //GM.DoPauseMonsters(false);
    }

    public override void AddListeners()
    {
        base.AddListeners();
        InputMaster.OnActionPressed += ButtonInput;
        InputMaster.OnActionReleased += ButtonRelease;
        InputMaster.LeftStickRelativeDirection += LeftStickDirection;
        InputMaster.RightStickDirection += RightStickDirection;
    }

    public override void RemoveListeners()
    {
        base.RemoveListeners();
        InputMaster.OnActionPressed -= ButtonInput;
        InputMaster.OnActionReleased -= ButtonRelease;
        InputMaster.LeftStickRelativeDirection -= LeftStickDirection;
        InputMaster.RightStickDirection -= RightStickDirection;
    }

    void LeftStickDirection(Vector2 direction)
    {
        Vector3 newPosition = Target.transform.position + (new Vector3(direction.x, 0, direction.y) * Time.unscaledDeltaTime * GM.Settings.MagicTargetSpeed) - Player.transform.position;
        newPosition = Vector3.ClampMagnitude(newPosition, GM.Settings.MagicRange);
        newPosition = Player.transform.position + newPosition;

        Target.position = newPosition;

    }

    void RightStickDirection(Vector2 direction)
    {
        GM.DungeonCam.m_XAxis.Value = direction.x * GM.Settings.CameraSpeed;
    }

    void ButtonInput(InputMaster.ButtonInput button, InputMaster.ButtonModifier modifier)
    {
        switch(button)
        {
            case InputMaster.ButtonInput.Circle:
                //Player.InputDodge(-Player.transform.forward.ConvertToV2());
                //Player.InputCharge(0);
                GM.ChangeState<GMSWalk>();
                break;
        }
    }

    void ButtonRelease(InputMaster.ButtonInput button, InputMaster.ButtonModifier modifier)
    {
        switch (button)
        {
            case InputMaster.ButtonInput.L1:
                Player.InputCast(GetTargetPosition());
                Player.Mover.InstantFace((Target.transform.position - Player.transform.position).ConvertToV2());
                GM.ChangeState<GMSWalk>();
                break;
            case InputMaster.ButtonInput.R1:
                Player.InputSpecial();
                Player.Mover.InstantFace((Target.transform.position - Player.transform.position).ConvertToV2());
                GM.ChangeState<GMSWalk>();
                break;

        }
    }

    private Vector3 GetTargetPosition()
    {
        RaycastHit hit;
        if(Physics.Raycast(Target.transform.position + (Vector3.up * 1000), Vector3.down, out hit, 5000, LayerMask.GetMask(new string[] { "Default", "Level" })))
        {
            //Debug.Log("Got a hit");
            return hit.point;
        }
        else
        {
            //Debug.Log("No Hit");
            return Target.position;
        }

    }

    public override void Tick()
    {
        GM.HKPlayer.Mover.SlerpFace((Target.transform.position - Player.transform.position).ConvertToV2());
    }
}
