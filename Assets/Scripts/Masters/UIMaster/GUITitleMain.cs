﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GUITitleMain : MonoBehaviour
{
    public enum ScreenState
    {
        MAIN, STORY, OPTIONS, CREDITS
    }

    public enum OptionSelect
    {
        OPTION
    }

    int mainSelection;

    int sessionSelection;

    ScreenState state;
    public ScreenState State => state;
    public int SessionSelection => sessionSelection;

    Color dimmerBaseColor;
    Color dimmerDimColor;
    [SerializeField] UIButtonCollection mainTitleButtons;
    [SerializeField] Image mainTitleDimmer;
    [SerializeField] GameObject sessionPreviewBox;
    [SerializeField] UIButtonCollection sessionSelect;
    [SerializeField] TMPro.TextMeshProUGUI sessionPreviewText;
    [SerializeField] Image sessionPreviewImage;
    [SerializeField] TMPro.TextMeshProUGUI sessionPreviewCompl;
    [SerializeField] GameObject optionsScreen;
    [SerializeField] GameObject creditsScreen;

    CanvasGroup cg;
    // Start is called before the first frame update
    void Start()
    {
        cg = GetComponent<CanvasGroup>();
        state = ScreenState.MAIN;
        mainSelection = sessionSelection = 0;
        cg.alpha = 0;
        dimmerBaseColor = mainTitleDimmer.color;
        dimmerDimColor = new Color(dimmerBaseColor.r, dimmerBaseColor.g, dimmerBaseColor.b, 0.6f);
        LeanTween.alphaCanvas(cg, 1, 1.5f);
    }

    public void End()
    {
        LeanTween.alphaCanvas(cg, 0, 1.5f);
    }

    public void SelectDown()
    {
        switch (state)
        {
            case ScreenState.MAIN:
                mainSelection++;
                if (mainSelection > mainTitleButtons.Count - 1) mainSelection = mainTitleButtons.Count - 1;
                mainTitleButtons.SelectButton(mainSelection);
                break;
            case ScreenState.STORY:
                sessionSelection++;
                if (sessionSelection > sessionSelect.Count - 1) sessionSelection = sessionSelect.Count - 1;
                sessionSelect.SelectButton(sessionSelection);
                break;
            case ScreenState.OPTIONS:
                break;
            case ScreenState.CREDITS:
                break;
        }
    }

    public void SelectUp()
    {
        switch (state)
        {
            case ScreenState.MAIN:
                mainSelection--;
                if (mainSelection < 0) mainSelection = 0;
                mainTitleButtons.SelectButton(mainSelection);
                break;
            case ScreenState.STORY:
                sessionSelection--;
                if (sessionSelection < 0) sessionSelection = 0;
                sessionSelect.SelectButton(sessionSelection);
                break;
            case ScreenState.OPTIONS:
                break;
            case ScreenState.CREDITS:
                break;
        }
    }


    public void SelectScreenState(ScreenState newState)
    {
        state = newState;
        UpdateScreenState();
    }

    void UpdateScreenState()
    {
        switch(state)
        {
            case ScreenState.MAIN:
                sessionSelect.gameObject.SetActive(false);
                sessionPreviewBox.SetActive(false);
                mainTitleDimmer.color = dimmerBaseColor;
                mainTitleButtons.SelectButton(mainSelection);
                optionsScreen.SetActive(false);
                creditsScreen.SetActive(false);
                break;
            case ScreenState.STORY:
                mainTitleDimmer.color = dimmerDimColor;
                sessionSelect.gameObject.SetActive(true);
                sessionPreviewBox.SetActive(true);
                sessionSelection = 0;
                sessionSelect.SelectButton(sessionSelection);
                break;
            case ScreenState.OPTIONS:
                mainTitleDimmer.color = dimmerDimColor;
                optionsScreen.SetActive(true);
                break;
            case ScreenState.CREDITS:
                mainTitleDimmer.color = dimmerDimColor;
                creditsScreen.SetActive(true);
                break;
        }
    }

    public void SelectCurrentOption()
    {
        switch(state)
        {
            case ScreenState.MAIN:
                //0: story
                //1: options
                //2: credits

                if (mainSelection == 0) SelectScreenState(ScreenState.STORY);
                if (mainSelection == 1) SelectScreenState(ScreenState.OPTIONS);
                if (mainSelection == 2) SelectScreenState(ScreenState.CREDITS);

                break;
            case ScreenState.STORY:
                break;
            case ScreenState.OPTIONS:
                break;
            case ScreenState.CREDITS:
                break;
        }
    }
}
