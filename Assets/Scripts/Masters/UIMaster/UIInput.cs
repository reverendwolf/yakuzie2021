﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface UIInput
{
    void ButtonInput(InputMaster.ButtonInput button, InputMaster.ButtonModifier modifier);

}
