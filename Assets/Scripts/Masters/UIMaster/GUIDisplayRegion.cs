﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GUIDisplayRegion : MonoBehaviour
{
    TMPro.TextMeshProUGUI regionLabel;
    [SerializeField] Vector2 hidePosition;
    Vector2 startPosition;
    private void Start()
    {
        startPosition = GetComponent<RectTransform>().anchoredPosition;
        regionLabel = GetComponentInChildren<TMPro.TextMeshProUGUI>();
        //HideRegion();
        GetComponent<RectTransform>().anchoredPosition = hidePosition;
    }

    public void ShowMessage(string message)
    {        
        regionLabel.text = message;
        LeanTween.move(GetComponent<RectTransform>(), startPosition, 0.5f).setEaseInOutQuad().setOnComplete(HideRegion);
    }

    public void HideRegion()
    {
        LeanTween.move(GetComponent<RectTransform>(), hidePosition, 0.75f).setEaseInOutQuad().setDelay(4f);
    }
}
