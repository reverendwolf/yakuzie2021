﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GUIPauseMenu : MonoBehaviour, UIInput
{
    private enum MenuOperation
    {
        Continue,
        SaveNQuit
    }
    MenuOperation selected;

    [SerializeField] TMPro.TextMeshProUGUI[] menuTexts;

    void OnEnable()
    {
        selected = 0;
        UpdateTexts();
    }

    public void ButtonInput(InputMaster.ButtonInput button, InputMaster.ButtonModifier modifier)
    {
        //throw new System.NotImplementedException();
        switch(button)
        {
            case InputMaster.ButtonInput.Up:
                if(selected != MenuOperation.Continue) selected = (MenuOperation)((int)selected - 1);
                UpdateTexts();
                break;
            case InputMaster.ButtonInput.Down:
                if(selected != MenuOperation.SaveNQuit) selected = (MenuOperation)((int)selected + 1);
                UpdateTexts();
                break;
            case InputMaster.ButtonInput.Cross:
                DoMenu();
                break;
            case InputMaster.ButtonInput.Circle:
                selected = 0;
                UpdateTexts();
                break;
        }
    }

    void UpdateTexts()
    {
        foreach(TMPro.TextMeshProUGUI t in menuTexts)
        {
            t.color = Color.gray;
        }

        menuTexts[(int)selected].color = Color.white;
    }

    void DoMenu()
    {

    }
}
