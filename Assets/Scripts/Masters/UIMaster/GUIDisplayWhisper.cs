﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GUIDisplayWhisper : MonoBehaviour
{
    TMPro.TextMeshProUGUI whisperLabel;
    CanvasGroup cg;
    RectTransform rt;
    Vector3 startingPosition;
    //Vector3 hidePosition;
    Vector3 trackPosition;
    public bool Ready => cg.alpha == 0;
    private void Start()
    {
        whisperLabel = GetComponentInChildren<TMPro.TextMeshProUGUI>();
        cg = GetComponent<CanvasGroup>();
        rt = GetComponent<RectTransform>();
        startingPosition = rt.position;
        cg.alpha = 0;
        rt.localScale = Vector3.zero;
    }

    public void DisplayWhisper(string phrase, Vector3 whisperPosition)
    {
        trackPosition = whisperPosition;
        LeanTween.cancel(this.gameObject);
        CancelInvoke();
        transform.localScale = Vector3.zero;
        whisperLabel.text = phrase;
        LeanTween.alphaCanvas(cg, 1, 0.5f);
        LeanTween.scale(rt, Vector3.one, 0.5f).setEaseInOutElastic();
        Invoke(nameof(HideWhisper), 3f + phrase.Length * 0.1f);
    }

    public void HideWhisper()
    {
        LeanTween.cancel(this.gameObject);
        LeanTween.alphaCanvas(cg, 0, 1.5f);
        LeanTween.scale(rt, Vector3.zero, 1.5f).setEaseInCubic();
    }

    private void Update()
    {
        if(trackPosition != Vector3.zero) transform.position = Camera.main.WorldToScreenPoint(trackPosition);
    }
}
