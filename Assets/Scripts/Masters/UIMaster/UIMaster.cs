﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UIMaster : MonoBehaviour
{
    private static UIMaster _instance;
    public static UIMaster I
    {
        get
        {
            if (!_instance)
            {
                _instance = GameObject.FindObjectOfType(typeof(UIMaster)) as UIMaster;
            }

            if (!_instance)
            {
                Debug.LogError("There is no UIMaster object!");
            }

            return _instance;
        }
    }

    public void OnEnable()
    {
        if (I != null && I != this)
            DestroyImmediate(this.gameObject);
    }

    

    //public GUIDisplayVerb VerbDisplay => verbDisplay;
    [SerializeField] Canvas pauseCanvas;
    [SerializeField] Canvas combatCanvas;
    [SerializeField] Canvas mainCanvas;

    [SerializeField] CanvasGroup fadeToBlack;
    [SerializeField] GUIDisplayVerb verbDisplay;
    [SerializeField] GUIDisplayRegion regionDisplay;
    [SerializeField] GUIDisplayRegion topMessage;
    [SerializeField] GUIWhisper whisperDisplay;
    [SerializeField] GUITitleMain titleMain;
    public GUITitleMain Title => titleMain;

    private void Start()
    {
        pauseCanvas.gameObject.SetActive(false);
        combatCanvas.gameObject.SetActive(false);
        mainCanvas.gameObject.SetActive(false);
    }

    public IEnumerator FadeToBlack()
    {
        yield return StartCoroutine(DoFade(false));
    }

    public IEnumerator FadeToView()
    {
        yield return StartCoroutine(DoFade(true));
    }

    public void SnapToBlack()
    {
        fadeToBlack.alpha = 1;
    }

    public void SnapToView()
    {
        fadeToBlack.alpha = 0;
    }

    IEnumerator DoFade(bool canViewGame)
    {
        float a = canViewGame ? 0f : 1f;
        while(fadeToBlack.alpha != a)
        {
            fadeToBlack.alpha = Mathf.MoveTowards(fadeToBlack.alpha, a, Time.deltaTime);
            yield return null;
        }
    }

    public void ShowVerb(GUIDisplayVerb.PossibleVerbs verb)
    {
        verbDisplay.DisplayVerb(verb);
    }

    public void HideVerb()
    {
        verbDisplay.HideVerb();
    }

    public void SuspendVerb(bool hide)
    {
        verbDisplay.gameObject.SetActive(!hide);
    }

    public void ShowRegion(MapRegion region)
    {
        regionDisplay.ShowMessage(region.RegionName);
    }

    public void ShowCurrentObjective()
    {
        Debug.Log("Showing current oebjective...");
        //MissionGoal mt = MapMaster.I.Quest.CurrentMission;
        //regionDisplay.ShowMessage(mt.GoalName);
    }

    public void ShowTopMessage(string message)
    {
        topMessage.ShowMessage(message);
    }

    public void ShowPauseMenu(bool show)
    {
        if (pauseCanvas != null)
        {
            pauseCanvas.gameObject.SetActive(show);
            //combatCanvas.enabled = !show;
            mainCanvas.gameObject.SetActive(!show);
        }
    }

    public void ShowWhisper(string whisper, Vector3 wPosition)
    {
        whisperDisplay.ShowWhisper(whisper, wPosition);
    }

    public void ShowTitleScreen(bool show)
    {
        if(show)
        {
            titleMain.gameObject.SetActive(true);
            titleMain.SelectScreenState(GUITitleMain.ScreenState.MAIN);
        }
        else
        {
            titleMain.gameObject.SetActive(false);
        }
    }

    public void ShowSessionScreen(bool show)
    {
        combatCanvas.gameObject.SetActive(show);
        mainCanvas.gameObject.SetActive(show);
    }
}
