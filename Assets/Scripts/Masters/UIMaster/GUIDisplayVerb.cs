﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GUIDisplayVerb : MonoBehaviour
{
    public enum PossibleVerbs
    {
        Look,
        Talk,
        Read,
        Listen,
        Investigate,
        Go,
        Push,
        Pull,
        Open,
        Take,
        Fish,
        Enter
    }

    TMPro.TextMeshProUGUI verbLabel;
    CanvasGroup cg;
    Vector3 startingPosition;
    private void Start()
    {
        verbLabel = GetComponentInChildren<TMPro.TextMeshProUGUI>();
        HideVerb();
    }

    public void DisplayVerb(PossibleVerbs verbID)
    {
        verbLabel.text = "<link=\"wave\">" + verbID.ToString() + "</link>";
        //verbLabel.text = "<wave>" + verbID.ToString() + "</wave>";
        LeanTween.move(GetComponent<RectTransform>(), new Vector2(-32, 32), 0.35f).setEaseOutElastic();
        //cg.alpha = 1
    }

    public void DisplayVerb(string verb)
    {
        verbLabel.text = "<link=\"wave\">" + verb + "</link>";
        //verbLabel.text = "<wave>" + verb + "</wave>";
        LeanTween.move(GetComponent<RectTransform>(), new Vector2(-32, 32), 0.35f);
        //cg.alpha = 1;
    }

    public void HideVerb()
    {
        LeanTween.move(GetComponent<RectTransform>(), new Vector2(-32, -100), 0.35f).setEaseInExpo(); ;
        //cg.alpha = 0;
    }
}
