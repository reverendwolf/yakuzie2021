﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GUIPauseChar : MonoBehaviour, UIInput
{
    [SerializeField] string[] moveClasses;
    [SerializeField] TMPro.TextMeshProUGUI[] moveLabels;
    [SerializeField] TMPro.TextMeshProUGUI[] characterLabels;
    HKPlayerData pd;
    GameData data;

    void Awake()
    {
        pd = GameMaster.I.PlayerData;
        characterLabels[0].text = pd.DisplayName;
        characterLabels[1].text = pd.JobName;
    }

    void OnEnable()
    {
        data = GameMaster.I.SaveData;
        UpdateMoveLabels();
    }

    public void ButtonInput(InputMaster.ButtonInput button, InputMaster.ButtonModifier modifier)
    {
        //throw new System.NotImplementedException();
    }

    void UpdateMoveLabels()
    {
        for (int i = 0; i < moveLabels.Length; i++)
        {
            //moveLabels[i].text = moveClasses[i] + ":\n" + (data.Unlocks.AllAbilities[i] ? tree.Moves[i] : "---");
            //moveLabels[i].text = (data.Skills[i] ? tree.Moves[i] : "---");
            Debug.Log(i);
            Debug.Log(pd.Attacks[i].DisplayName);
            moveLabels[i].text = pd.Attacks[i].DisplayName;
            //moveLabels[i].text = "---";
        }
    }
}
