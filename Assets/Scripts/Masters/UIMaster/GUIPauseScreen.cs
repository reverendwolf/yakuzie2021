﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GUIPauseScreen : MonoBehaviour
{
    private enum Screen
    {
        Menu,
        Quest,
        Character,
        Treasure
    }
    Screen selected;
    [SerializeField] GUIPauseMenu menu;
    [SerializeField] GUIPauseQuest quest;
    [SerializeField] GUIPauseChar chara;
    [SerializeField] GUIPauseTreasure treas;
    [SerializeField] TMPro.TextMeshProUGUI leftNavLabel;
    [SerializeField] TMPro.TextMeshProUGUI rightNavLabel;
    [SerializeField] GameObject leftNavObj;
    [SerializeField] GameObject rightNavObj;
    GameObject[] screens;

    private void Awake()
    {
        screens = new GameObject[] { menu.gameObject, quest.gameObject, chara.gameObject, treas.gameObject };
    }

    private void OnEnable()
    {
        InputMaster.OnActionPressed += ButtonInput;
        selected = Screen.Quest;
        UpdateScreens();
    }

    private void OnDisable()
    {
        InputMaster.OnActionPressed -= ButtonInput;
    }

    void ButtonInput(InputMaster.ButtonInput button, InputMaster.ButtonModifier modifier)
    {
        Screen oldScreen = selected;
        switch(button)
        {
            case InputMaster.ButtonInput.L1:
                if(selected != Screen.Menu) selected = (Screen)((int)selected - 1);
                break;
            case InputMaster.ButtonInput.R1:
                if (selected != Screen.Treasure) selected = (Screen)((int)selected + 1);
                break;
        }

        if(oldScreen != selected)
        {
            UpdateScreens();
        }

        switch(selected)
        {
            case Screen.Menu:
                menu.ButtonInput(button, modifier);
                break;
            case Screen.Quest:
                quest.ButtonInput(button, modifier);
                break;
            case Screen.Character:
                chara.ButtonInput(button, modifier);
                break;
            case Screen.Treasure:
                treas.ButtonInput(button, modifier);
                break;
        }
    }

    void UpdateScreens()
    {
        foreach(GameObject g in screens)
        {
            g.SetActive(false);
        }

        screens[(int)selected].SetActive(true);
        switch(selected)
        {
            case Screen.Menu:
                leftNavObj.SetActive(false);
                rightNavObj.SetActive(true);
                rightNavLabel.text = "Quest";
                break;
            case Screen.Quest:
                leftNavObj.SetActive(true);
                leftNavLabel.text = "Menu";
                rightNavObj.SetActive(true);
                rightNavLabel.text = "Character";
                break;
            case Screen.Character:
                leftNavObj.SetActive(true);
                leftNavLabel.text = "Quest";
                rightNavObj.SetActive(true);
                rightNavLabel.text = "Treasures";
                break;
            case Screen.Treasure:
                leftNavObj.SetActive(true);
                leftNavLabel.text = "Character";
                rightNavObj.SetActive(false);
                break;
        }
    }
}
