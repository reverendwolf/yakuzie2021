﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GUIPauseTreasure : MonoBehaviour, UIInput
{
    private enum Selected
    {
        Weapon,
        Heal,
        Special,
        Super,
        Cast,
        Ultimate
    }

    Selected selected;

    [SerializeField] GUIPauseTreasureBox[] skillTreasures = new GUIPauseTreasureBox[8];
    [SerializeField] GUIPauseTreasureBox[] collectibles = new GUIPauseTreasureBox[4];

    [SerializeField] TMPro.TextMeshProUGUI treasureLabel;
    [SerializeField] TMPro.TextMeshProUGUI treasureDesc;

    HKPlayerData hkpd;
    GameData data;

    void Awake()
    {
        hkpd = GameMaster.I.PlayerData;
    }

    void OnEnable()
    {
        data = GameMaster.I.SaveData;
        selected = Selected.Weapon;
        UpdateSelected();
        UpdateTreasureInfo();
        GetTreasureLabels();
    }

    public void ButtonInput(InputMaster.ButtonInput button, InputMaster.ButtonModifier modifier)
    {
        //throw new System.NotImplementedException();
        switch (button)
        {
            case InputMaster.ButtonInput.Up:
                if (selected != Selected.Weapon) selected = (Selected)((int)selected - 1);
                UpdateTreasureInfo();
                UpdateSelected();
                break;
            case InputMaster.ButtonInput.Down:
                if (selected != Selected.Ultimate) selected = (Selected)((int)selected + 1);
                UpdateTreasureInfo();
                UpdateSelected();
                break;
        }
    }

    void GetTreasureLabels()
    {
        for (int i = 0; i < skillTreasures.Length; i++)
        {
            //skillTreasures[i].UpdateDisplay(data.Skills[i] ? tree.AllTreasures[i].Name : "---");
            skillTreasures[i].UpdateDisplay(hkpd.KeyItems[i].DisplayName);
        }

        for (int i = 0; i < collectibles.Length; i++)
        {
            //collectibles[i].UpdateDisplay("6/9", data.ShardLevels[i] + "");
        }
    }

    void UpdateTreasureInfo()
    {
        if(data.Skills[(int)selected])
        {
            //treasureLabel.text = tree.AllTreasures[(int)selected].Name;
            //treasureDesc.text = tree.AllTreasures[(int)selected].Desc;
            treasureLabel.text = hkpd.KeyItems[(int)selected].DisplayName;
            treasureDesc.text = hkpd.KeyItems[(int)selected].Description;
        }
        else
        {
            treasureLabel.text = "No Information";
            treasureDesc.text = "You have not found this Treasure yet.";
        }
    }

    void UpdateSelected()
    {
        foreach(GUIPauseTreasureBox b in skillTreasures)
        {
            b.Shadow();
        }

        skillTreasures[(int)selected].Highlight();
    }
}
