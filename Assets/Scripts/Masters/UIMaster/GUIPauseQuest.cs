﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GUIPauseQuest : MonoBehaviour, UIInput
{
    [SerializeField] TMPro.TextMeshProUGUI sessionLabel;
    [SerializeField] TMPro.TextMeshProUGUI questLabel;

    public void ButtonInput(InputMaster.ButtonInput button, InputMaster.ButtonModifier modifier)
    {
        //throw new System.NotImplementedException();
    }

    private void OnEnable()
    {
        //MissionMaster.MissionObjective mo = MissionMaster.I.GetCurrentObjective();
        //MissionGoal goal = MapMaster.I.Quest.CurrentMission;
        //questLabel.text = string.Format("{0}/{1}: {2}", MapMaster.I.Quest.CurrentProgress, MapMaster.I.Quest.TotalGoals, goal.GoalName);
        //questDescript.text = goal.GoalDesc;
        //questDescript.text = MapMaster.I.Quest.GetActiveObjectives();
        questLabel.text = GameMaster.I.CurrentMap.SessionMaster.GetActiveQuests();
        sessionLabel.text = GameMaster.I.CurrentMap.SessionMaster.SessionTitle;
    }
}
