﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GUIWhisper : MonoBehaviour
{ 
    [SerializeField] GUIDisplayWhisper display;

    public void ShowWhisper(string whisper, Vector3 whisperPosition)
    {
        display.DisplayWhisper(whisper, whisperPosition);
    }
}
