﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class GUIPauseTreasureBox : MonoBehaviour
{
    [SerializeField] Image icon;
    [SerializeField] TMPro.TextMeshProUGUI label;
    [SerializeField] TMPro.TextMeshProUGUI extra;
    Image box;

    public void UpdateDisplay(string labelText)
    {
        label.text = labelText;
    }

    public void UpdateDisplay(Sprite sprite, string labelText)
    {
        icon.sprite = sprite;
        UpdateDisplay(labelText);
    }

    public void UpdateDisplay(string labelText, string extraText)
    {
        label.text = labelText;
        extra.text = extraText;
    }

    public void Highlight()
    {
        if (box == null) box = GetComponent<Image>();
        box.color = Color.white;
    }

    public void Shadow()
    {
        if (box == null) box = GetComponent<Image>();
        box.color = Color.gray;
    }

}
