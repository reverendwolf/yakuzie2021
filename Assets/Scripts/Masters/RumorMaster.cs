﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RumorMaster : MonoBehaviour
{
    [SerializeField] [TextArea] string[] rumors;
    public string[] Rumors => rumors;
}
