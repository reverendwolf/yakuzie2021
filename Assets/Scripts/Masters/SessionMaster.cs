﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using NaughtyAttributes;

public class SessionMaster : MonoBehaviour
{
    [SerializeField] string sessionTitle;
    public string SessionTitle => sessionTitle;

    [System.Serializable]
    public class Quest
    {
        public string questTitle;
        public GameFlag gameFlag;
        [TextArea]
        public string[] phases;
        [TextArea]
        public string completion;
        public string[] Strings
        {
            get
            {
                List<string> _return = new List<string>();
                _return.Add(questTitle);
                _return.AddRange(phases);
                _return.Add(completion);
                return _return.ToArray();
            }
        }
    }

    [ReorderableList] [SerializeField] List<Quest> editorRegistry = new List<Quest>();
    Dictionary<string, string[]> questRegistry;

    private void Awake()
    {
        questRegistry = new Dictionary<string, string[]>();
        foreach(Quest q in editorRegistry)
        {
            questRegistry.Add(q.gameFlag.name, q.Strings);
        }
    }

    public string GetActiveQuests()
    {
        string _return = "";
        foreach(string q in questRegistry.Keys)
        {
            int phase = GameMaster.I.SaveData.GetFlag(q) / 10;
            if(phase > 0 && phase < questRegistry[q].Length)
            {
                _return += "<color=#FFFFFF><size=24>" + questRegistry[q][0] + "</size></color>\n";
                if(phase < 255)
                {
                    _return += "<color=#999999><size=20>" + questRegistry[q][phase] + "</size></color>\n";
                }
                else
                {
                    _return += questRegistry[q][questRegistry[q].Length - 1] + "\n";
                }
            }
        }
        return _return;
    }
}
