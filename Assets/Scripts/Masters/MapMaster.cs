﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MapMaster : MonoBehaviour
{
    public delegate void Signal();
    public event Signal OnInitialize;

    private static MapMaster _instance;
    public static MapMaster I
    {
        get
        {
            if (!_instance)
            {
                _instance = GameObject.FindObjectOfType(typeof(MapMaster)) as MapMaster;
            }

            if (!_instance)
            {
                Debug.LogError("There is no MapMaster object!");
            }

            return _instance;
        }
    }

    public void OnEnable()
    {
        if (I != null && I != this)
            DestroyImmediate(this.gameObject);
    }

    public GameMaster GM => GameMaster.I;

    public string MapName => mapName;
    [SerializeField] string mapName = "New Screen";
    [SerializeField] List<MapRegion> allRegions;
    int currentRegion;
    public MapRegion CurrentRegion => allRegions[currentRegion];
    [SerializeField] MapRegion chapterRegionStart;
    [SerializeField] Transform chapterStartPosition;
    public Vector3 StartPosition => chapterStartPosition.position;
    [SerializeField] GameObject defaultLighting;
    [SerializeField] Fungus.Flowchart missionChart;
    public Fungus.Flowchart MissionChart => missionChart;
    [SerializeField] SessionData missionQuest;
    public SessionData Quest => missionQuest;
    [SerializeField] SessionMaster sessionMaster;
    public SessionMaster SessionMaster => sessionMaster;
    
    //public CombatModuleMaster CombatModule => GetComponentInChildren<CombatModuleMaster>();
    private void Start()
    {
        if(missionQuest != null)
        {
            //missionQuest.InitializeQuest();
            //missionQuest.UpdateObjective(0, MissionQuest.ObjectiveStatus.Active);
        }
        GMSMissionStart.OnMissionStart += InitializeMap;
    }

    public void InitializeMap()
    {
        Debug.Log("Initialize Map Master");
        GMSMissionStart.OnMissionStart -= InitializeMap;
        allRegions.Clear();
        allRegions.AddRange(FindObjectsOfType<MapRegion>());
        GameMaster.I.RegisterMapMaster(this);
        OnInitialize?.Invoke();
        currentRegion = allRegions.IndexOf(chapterRegionStart);
        
        for (int i = 0; i < allRegions.Count; i++)
        {
            if (i == currentRegion) allRegions[i].gameObject.SetActive(true);
            else allRegions[i].gameObject.SetActive(false);
        }
        //defaultLighting?.SetActive(!CurrentRegion.Inside);
        //RenderSettings.ambientSkyColor = CurrentRegion.AmbientColor;
        CurrentRegion.RegionEnter();
    }

    public void RegionTransision(MapRegion destination)
    {
        if (allRegions.Contains(destination) == false) { Debug.LogWarning("Destination region not registered with mapmaster"); return; }
        CurrentRegion.RegionLeave();
        CurrentRegion.gameObject.SetActive(false);
        destination.gameObject.SetActive(true);

        //defaultLighting?.SetActive(!destination.Inside);
        //RenderSettings.ambientSkyColor = destination.AmbientColor;

        currentRegion = allRegions.IndexOf(destination);
        CurrentRegion.RegionEnter();
    }   
}
