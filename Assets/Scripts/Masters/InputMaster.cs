﻿using InControl;
using UnityEngine;
using System.Collections.Generic;

public class InputMaster : MonoBehaviour
{
    private static InputMaster _instance;
    public static InputMaster I
    {
        get
        {
            if (!_instance)
            {
                _instance = GameObject.FindObjectOfType(typeof(InputMaster)) as InputMaster;
            }

            if (!_instance)
            {
                Debug.LogError("There is no InputMaster object!");
            }

            return _instance;
        }
    }

    public void OnEnable()
    {
        if (I != null && I != this)
            DestroyImmediate(this.gameObject);
    }

    const int SMOOTHING_FRAMES = 5;
    public enum ButtonInput { None, Up, Down, Left, Right, Square, Triangle, Circle, Cross, L1, R1, L1R1, L2, R2, Start }
    public enum ButtonModifier { None, L1, R1 }
    public enum SpecialAction { None, Confirm, Cancel}

    public delegate void SpecificInput();
    public delegate void ActionButtonSingle(ButtonInput button, InputMaster.ButtonModifier modifier = ButtonModifier.None);
    public delegate void ActionButtonHold(ButtonInput button, float holdTime, InputMaster.ButtonModifier modifier = ButtonModifier.None);
    public delegate void StickInput(Vector2 direction);

    public static event ActionButtonSingle OnActionPressed;
    public static event ActionButtonSingle OnActionReleased;
    public static event SpecificInput OnConfirmActionPressed;
    public static event SpecificInput OnCancelActionPressed;
    public static event StickInput LeftStickDirection;
    public static event StickInput LeftStickRelativeDirection;
    public static event StickInput RightStickDirection;

    public const float THRESHOLD = 1.2f;
    public const float DEADZONE = 0.1f;

    public ButtonInput lastButton;
    public float holdTime;
    public int holdFrames;
    public bool holding;
    public ButtonModifier actionModifier;

    public Vector2[] leftStickInput;
    public Vector2 LeftStickAverage { get { return (leftStickInput[0] + leftStickInput[1] + leftStickInput[2]) / 3f; } }
    public Vector2 rightStickInput;

    int stickInputFrame;

    private void Start()
    {
        leftStickInput = new Vector2[SMOOTHING_FRAMES];
    }


    // Update is called once per frame
    void LateUpdate()
    {
        lastButton = ButtonInput.None;
        InputDevice ad = InputManager.ActiveDevice;

        CheckModifiers(KeyCode.Y, ad.LeftBumper, KeyCode.I, ad.RightBumper);
        NewInput(KeyCode.F, ad.Action3, ButtonInput.Square);
        NewInput(KeyCode.R, ad.Action4, ButtonInput.Triangle);
        NewInput(KeyCode.E, ad.Action1, ButtonInput.Cross);
        NewInput(KeyCode.LeftShift, ad.Action2, ButtonInput.Circle);

        NewInput(KeyCode.UpArrow, ad.DPadUp, ButtonInput.Up);
        NewInput(KeyCode.DownArrow, ad.DPadDown, ButtonInput.Down);
        NewInput(KeyCode.LeftArrow, ad.DPadLeft, ButtonInput.Left);
        NewInput(KeyCode.RightArrow, ad.DPadRight, ButtonInput.Right);

        NewInput(KeyCode.U, ad.LeftBumper, ButtonInput.L1, false);
        NewInput(KeyCode.O, ad.RightBumper, ButtonInput.R1, false);

        NewInput(KeyCode.J, ad.LeftTrigger, ButtonInput.L2);
        NewInput(KeyCode.L, ad.RightTrigger, ButtonInput.R2);

        NewInput(KeyCode.Escape, ad.Command, ButtonInput.Start);

        float inputVal = Input.GetKey(KeyCode.LeftShift) ? 0.15f : 1f;
        float kblh = Input.GetKey(KeyCode.A) ? -inputVal : Input.GetKey(KeyCode.D) ? inputVal : 0;
        float kblv = Input.GetKey(KeyCode.S) ? -inputVal : Input.GetKey(KeyCode.W) ? inputVal : 0;
        Vector2 kbLInput = new Vector2(kblh, kblv);

        float kbrh = Input.GetKey(KeyCode.Keypad4) ? -1 : Input.GetKey(KeyCode.Keypad6) ? 1 : 0;
        float kbrv = Input.GetKey(KeyCode.Keypad2) ? -1 : Input.GetKey(KeyCode.Keypad8) ? 1 : 0;
        Vector2 kbRInput = new Vector2(kbrh, kbrv);

        if (kbLInput == Vector2.zero)
        {
            leftStickInput[stickInputFrame] = ad.LeftStick.LastValue;
        }
        else
        {
            leftStickInput[stickInputFrame] = kbLInput;
        }

        //leftStickInput[stickInputFrame] = ad.LeftStick.LastValue;
        Vector2 leftSmooth = Vector2.zero;
        for (int i = 0; i < leftStickInput.Length; i++)
        {
            leftSmooth += leftStickInput[i];
        }
        leftSmooth /= leftStickInput.Length;

        if (Mathf.Abs(leftSmooth.x) < DEADZONE) leftSmooth.x = 0;
        if (Mathf.Abs(leftSmooth.y) < DEADZONE) leftSmooth.y = 0;
        LeftStickDirection?.Invoke(leftSmooth);


        LeftStickRelativeDirection?.Invoke(Camera.main.RelativeDirection(leftSmooth));

        stickInputFrame++;
        stickInputFrame = stickInputFrame % SMOOTHING_FRAMES;

        if (kbRInput == Vector2.zero)
        {
            rightStickInput = ad.RightStick.LastValue;
        }
        else
        {
            rightStickInput = kbRInput;
        }
        //rightStickInput = ad.RightStick.LastValue;

        if (Mathf.Abs(rightStickInput.x) < DEADZONE) rightStickInput.x = 0;
        if (Mathf.Abs(rightStickInput.y) < DEADZONE) rightStickInput.y = 0;
        RightStickDirection?.Invoke(rightStickInput);
    }

    void NewInput(KeyCode key, InControl.InputControl control, ButtonInput button, bool sendModifier = true)
    {
        if(Input.GetKeyDown(key) || control.WasPressed)
        {
            //Debug.Log("Input: " + button.ToString() + " pressed. MOD: " + actionModifier.ToString());
            OnActionPressed?.Invoke(button, sendModifier ? actionModifier : ButtonModifier.None);
            lastButton = button;
        }
        else if(Input.GetKeyUp(key) || control.WasReleased)
        {
            //Debug.Log("Input: " + button.ToString() + " released MOD: " + actionModifier.ToString());
            OnActionReleased?.Invoke(button, sendModifier ? actionModifier : ButtonModifier.None);
        }
    }

    void CheckModifiers(KeyCode leftKey, InControl.InputControl leftCont, KeyCode rightKey, InControl.InputControl rightCont)
    {
        if(Input.GetKey(leftKey) || leftCont.IsPressed)
        {
            actionModifier = ButtonModifier.L1;
        }
        else if(Input.GetKey(rightKey) || rightCont.IsPressed)
        {
            actionModifier = ButtonModifier.R1;
        }
        else
        {
            actionModifier = ButtonModifier.None;
        }
    }

    public bool CheckLastInput(ButtonInput button)
    {
        return lastButton == button;
    }
}
