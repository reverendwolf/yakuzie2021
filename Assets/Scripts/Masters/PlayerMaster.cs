﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMaster : MonoBehaviour
{
    [SerializeField] HKSkillTree[] playerTrees;
    
    public HKSkillTree GetPlayerTree(int playerID)
    {
        return playerTrees[playerID];
    }
}
