﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu()]
public class GameMasterSettings : ScriptableObject
{

    [SerializeField] [Range(1f,5f)] float cameraSpeed;
    public float CameraSpeed => cameraSpeed;

    [SerializeField] [Range(1f, 10f)] float magicRange;
    [SerializeField] [Range(2f, 10f)] float magicTargetSpeed;
    public float MagicRange => magicRange;
    public float MagicTargetSpeed => magicTargetSpeed;

    [SerializeField] int meleeSpGain;
    public int MeleeSPGain => meleeSpGain;
}
