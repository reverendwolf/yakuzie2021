﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

public class MapObjectTools : Editor
{
    [MenuItem("Tools/Map Object Tools/Auto Wire Region Gates")]
    public static void AutoWireRegionGates()
    {
        List<MORegionGate> gates = new List<MORegionGate>();
        gates.AddRange(GameObject.FindObjectsOfType<MORegionGate>());

        Debug.Log("Found " + gates.Count + " gates.");

        for (int i = 0; i < gates.Count; i++)
        {
            string gateRegionName = gates[i].transform.parent.name.Split(' ')[0];
            Debug.Log("Gate Target Region " + i + ": " + gateRegionName);
            GameObject target = GameObject.Find(gateRegionName + " Entrance");
            if (target != null) Debug.Log("Found " + target.name);
            else Debug.Log("No Entrance Found for " + gateRegionName);
        }
    }
}
