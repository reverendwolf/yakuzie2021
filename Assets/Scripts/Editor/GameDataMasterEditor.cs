﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using UnityEngine.UIElements;

[CustomEditor(typeof(GameMasterData))]
public class GameDataMasterEditor : Editor
{
    public override void OnInspectorGUI()
    {
        base.OnInspectorGUI();
        EditorGUILayout.LabelField("Game Flags:");
        GameData data = ((GameMasterData)target).GameData;

        foreach(string s in data.GameState.Keys)
        {
            EditorGUILayout.LabelField(s + ": " + data.GameState[s]);
        }
    }
}
