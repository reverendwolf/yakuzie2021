﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameModuleBase : MBStateMachine
{
    public delegate void Callback();
    public delegate void IntSignal(int signal);

    public event Callback OnModuleStart;
    public event IntSignal OnModuleEnd;

    public virtual bool Running => false;

    public virtual void LaunchModule(HKPlayer player) { }

    public virtual void CompleteModule(int signal) { CallModuleEnd(signal); }

    protected void CallModuleEnd(int signal) { OnModuleEnd?.Invoke(signal); }
}
