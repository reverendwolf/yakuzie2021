﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Fungus;

[CommandInfo("Master Interfaces", "GM Load Session", "Send a command to the global Game Master to load a new mission/chapter")]
public class CommandGMLoadScene : Command
{
    public int sessionIndex;

    public override void OnEnter()
    {
        base.OnEnter();
        GameMaster.I.LoadSession(sessionIndex);
    }

    public override string GetSummary()
    {
        return "Load Session " + sessionIndex + "(Scene index " + (sessionIndex + 2) + ")";
    }
}
