﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Fungus.MenuDialog))]
public class MenuDialogueInterface : MonoBehaviour {

    Fungus.MenuDialog md;
    [SerializeField]
    int selectedIndex = 0;

	// Use this for initialization
	void Start () {
        md = GetComponent<Fungus.MenuDialog>();
        GetComponent<Canvas>().enabled = true;
        selectedIndex = -1;
	}

    void MenuStart(Fungus.MenuDialog menu)
    {
        if(md == menu)
        {
            InputMaster.OnActionPressed += ButtonInput;
            selectedIndex = 0;
        }
    }

    void MenuEnd(Fungus.MenuDialog menu)
    {
        if(md == menu)
        {
            InputMaster.OnActionPressed -= ButtonInput;
            selectedIndex = -1;
        }
    }
	
    private void OnEnable()
    {
        Fungus.MenuSignals.OnMenuStart += MenuStart;
        Fungus.MenuSignals.OnMenuEnd += MenuEnd;
    }

    private void OnDisable()
    {
        Fungus.MenuSignals.OnMenuStart -= MenuStart;
        Fungus.MenuSignals.OnMenuEnd -= MenuEnd;
    }

    public void ButtonInput(InputMaster.ButtonInput button, InputMaster.ButtonModifier modifier)
    {
        if (md.DisplayedOptionsCount <= 0)
            return;

        bool selectInput = false;
        if(button == InputMaster.ButtonInput.Up)
        {
            selectedIndex--;
            selectInput = true;
        }
        else if(button == InputMaster.ButtonInput.Down)
        {
            selectedIndex++;
            selectInput = true;
        }

        //Debug.Log("Int: " + md.CachedButtons[selectedIndex].interactable);

        if (selectInput)
        {
            if (selectedIndex < 0) selectedIndex = md.DisplayedOptionsCount - 1;
            else selectedIndex %= md.DisplayedOptionsCount;
            //selectedIndex = Mathf.Clamp(selectedIndex, 0, md.DisplayedOptionsCount - 1);
            md.CachedButtons[selectedIndex].Select();
        }
        
        if(button == InputMaster.ButtonInput.Cross)
        {
            if (md.CachedButtons[selectedIndex].interactable)
            {
                md.CachedButtons[selectedIndex].onClick.Invoke();
            }
            
        }
    }
}
