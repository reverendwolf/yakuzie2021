﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Fungus;

[CommandInfo("Direction", "Camera Control", "Tell the GameMaster to move the camera look target to a certain point.")]
public class CommandCameraControl : Command
{
    public Transform lookTarget;
    public float waitTime = 1f;
    public override void Execute()
    {
        base.Execute();

        if (lookTarget != null) GameMaster.I.CameraLookAt(lookTarget.position, waitTime);
        else GameMaster.I.CameraReset(waitTime);

        Invoke("Continue", waitTime);
    }

    

    public override string GetSummary()
    {
        return lookTarget == null ? "Reset camera position to zero" : "Look at Transform: " + lookTarget.name;
    }
}
