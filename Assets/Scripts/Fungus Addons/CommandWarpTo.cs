﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Fungus;
[CommandInfo("Direction", "Warp To", "Tell a Navmeshmover to warp to a target point.")]
public class CommandWarpTo : Command
{
     public NavmeshMover mover;
    public Transform targetTransform;

    public override void Execute()
    {
        base.Execute();
        Warp();
        Continue();
    }

    void Warp()
    {
        if (mover == GameMaster.I.HKPlayer.Mover)
        {
            GameMaster.I.WarpPlayer(targetTransform.position);
        }
        else
        {
            mover.TeleportTo(targetTransform.position);
        }
    }

    public override string GetSummary()
    {
        return mover == null ? "Error: No NavmeshMover to warp!" : targetTransform == null ? "Error: No target to move to!" : "Warp " + mover.name + " to " + targetTransform.name;
    }
}
