﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Fungus;

[CommandInfo("Master Interfaces", "UI Master", "Send a command to the global UIMaster")]
public class CommandUIMaster : Command
{
    public enum UIMCommand
    {
        FadeToBlack,
        FadeToView
    }

    public UIMCommand command;

    public override string GetSummary()
    {
        return command.ToString();
    }

    public override void OnEnter()
    {
        base.OnEnter();
        StartCoroutine(DoCommand());
    }

    IEnumerator DoCommand()
    {
        switch(command)
        {
            case UIMCommand.FadeToBlack:
                yield return StartCoroutine(UIMaster.I.FadeToBlack());
                yield return new WaitForSeconds(0.5f);
                break;
            case UIMCommand.FadeToView:
                yield return new WaitForSeconds(0.5f);
                yield return StartCoroutine(UIMaster.I.FadeToView());
                break;
        }
        Continue();
    }
}
