﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Fungus;
[CommandInfo("Direction", "Move To", "Tell a Navmeshmover to move towards a target point.")]
public class CommandMoveTo : Command
{
    public NavmeshMover mover;
    public Transform targetTransform;
    public float overrideSpeed;

    public override void Execute()
    {
        base.Execute();
        StartCoroutine(Run());
    }

    IEnumerator Run()
    {
        if (overrideSpeed > 0f)
            mover.SetSpeed(overrideSpeed);
        mover.MoveTo(targetTransform.position);
        while(mover.WalkingTo)
        {
            yield return null;
        }
        mover.ResetSpeed();
        Continue();
    }

    public override string GetSummary()
    {
        return targetTransform == null ? "Error: No target to move to!" : "Move to position of Transform: " + targetTransform.name;
    }
}
