﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Fungus;
[CommandInfo("RPG", "Set Game Flag", "Sets a Game Flag value in currently loaded GameData")]
public class CommandSetGameFlag : Command
{
    public GameFlag flag;
    public byte value;
    public Helper.Operation operation;

    public override void Execute()
    {
        base.Execute();

        byte curValue = GameMaster.I.SaveData.GetFlag(flag.name);

        switch(operation)
        {
            case Helper.Operation.ADD:
                GameMaster.I.SaveData.SetFlag(flag.name, (byte)(curValue + value));
                break;
            case Helper.Operation.SET:
                GameMaster.I.SaveData.SetFlag(flag.name, value);
                break;
            case Helper.Operation.SUB:
                GameMaster.I.SaveData.SetFlag(flag.name, (byte)(curValue - value));
                break;
        }

        Continue();
    }

    public override string GetSummary()
    {
        return flag == null ? "Error: No Game Flag to Set!" : operation.ToString() + " " + flag.name + " to " + value;
    }
}
