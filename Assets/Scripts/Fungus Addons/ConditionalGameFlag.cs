﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Fungus;

[CommandInfo("Flow", "If Flag", "Conditional to check a game flag avainst a value")]
public class ConditionalGameFlag : Condition
{
    public GameFlag flag;
    public byte value;
    public Helper.Comparison comparison;

    protected override bool EvaluateCondition()
    {
        if (flag == null) return false;

        byte curValue = GameMaster.I.SaveData.GetFlag(flag.name);

        switch(comparison)
        {
            case Helper.Comparison.GT:
                return curValue > value;
            case Helper.Comparison.LT:
                return curValue < value;
            case Helper.Comparison.EQ:
                return curValue == value;
            default:
                return false;
        }
    }

    public override string GetSummary()
    {
        return flag == null ? "No Flag Set!" : flag.name + " is " + comparison.ToString() + " " + value;
    }
}
