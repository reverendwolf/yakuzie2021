﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Fungus;
[CommandInfo("Flow", "If Zenny", "Conditional to check if Zenny exceeds a value")]
public class ConditionalZennyCount : Condition
{
    public int value;

    protected override bool EvaluateCondition()
    {
        int curValue = GameMaster.I.Wallet;

        return curValue >= value;
    }

    public override string GetSummary()
    {
        return "Exceeds: " + value;
    }
}
