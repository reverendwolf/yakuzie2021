﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Fungus;
[CommandInfo("RPG", "Modify Zenny", "Add or Deduct a value of Zenny from the player")]
public class CommandZennyPay : Command
{
    public int value;

    public override void Execute()
    {
        base.Execute();
        GameMaster.I.Wallet += value;
        Continue();
    }

    public override string GetSummary()
    {
        return (value >= 0 ? "+" : "" ) + value + " Zenny";
    }
}
