﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Fungus;

[CommandInfo("Flow", "If Skill Status", "Conditional to check the status of an unlockable skill")]
public class ConditionalSkillStatus : Condition
{
    public Helper.UnlockList skill;
    public Helper.SkillStatus status;

    protected override bool EvaluateCondition()
    {

        Helper.SkillStatus curValue = GameMaster.I.SaveData.SkillStatus[(int)skill];

        return curValue == status;
    }

    public override string GetSummary()
    {
        return "If " + skill.ToString() + " is " + status.ToString();
    }
}
